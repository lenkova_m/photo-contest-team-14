package com.company.web.photo_contest.services;

import com.company.web.photo_contest.models.Category;
import com.company.web.photo_contest.repositories.CategoryRepository;
import jakarta.persistence.EntityExistsException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.company.web.photo_contest.Helpers.createMockCategory;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CategoryServiceTests {

    @Mock
    CategoryRepository mockCategoryRepository;

    @InjectMocks
    CategoryServiceImpl mockCategoryService;

    @Test
    void getAll_Should_CallRepository() {
        //Arrange
        List<Category> categoryList = new ArrayList<>();
        categoryList.add(0, createMockCategory());
        categoryList.add(1, createMockCategory());
        categoryList.add(2, createMockCategory());

        when(mockCategoryRepository.findAll())
                .thenReturn(categoryList);

        //Act
        List<Category> result = mockCategoryService.getAll();

        //Assert
        Assertions.assertEquals(categoryList.size(), result.size());
    }

    @Test
    void getCategoryById_Should_CallRepository() {
        //Arrange
        Category mockCategory = createMockCategory();

        when(mockCategoryRepository.getCategoryByCategoryId(mockCategory.getCategoryId()))
                .thenReturn(Optional.of(mockCategory));

        //Act
        Category category = mockCategoryService.getCategoryById(mockCategory.getCategoryId());

        //Assert
        assertEquals(mockCategory, category);
    }

    @Test
    void getCategoryByName_Should_CallRepository() {
        //Arrange
        Category mockCategory = createMockCategory();

        when(mockCategoryRepository.getCategoryByName(mockCategory.getName()))
                .thenReturn(Optional.of(mockCategory));

        //Act
        Category category = mockCategoryService.getCategoryByName(mockCategory.getName());

        //Assert
        assertEquals(mockCategory, category);
    }

    @Test
    public void createCategory_Should_CallRepository_When_CategoryDoesNotExist() {
        // Arrange
        Category category = createMockCategory();
        when(mockCategoryRepository.getCategoryByName(category.getName()))
                .thenReturn(Optional.empty());

        when(mockCategoryRepository.save(category))
                .thenReturn(category);

        // Act
        Category createdCategory = mockCategoryService.createCategory(category);

        // Assert
        verify(mockCategoryRepository, times(1))
                .getCategoryByName(category.getName());

        verify(mockCategoryRepository, times(1))
                .save(category);
        assertEquals(category, createdCategory);
    }

    @Test
    public void createCategory_Should_Throw_WhenCategoryExists() {
        // Arrange
        Category category = createMockCategory();
        when(mockCategoryRepository.getCategoryByName(category.getName()))
                .thenReturn(Optional.of(category));

        //Act, Assert
        assertThrows(EntityExistsException.class,
                () -> mockCategoryService.createCategory(category));
    }

    @Test
    public void deleteCategory_Should_CallRepository() {
        // Arrange
        Category category = createMockCategory();

        // Act
        mockCategoryService.deleteCategory(category);

        // Assert
        verify(mockCategoryRepository, times(1)).delete(category);
    }

    @Test
    public void filterByName_Should_CallRepository() {
        // Arrange
        String name = "category";
        List<Category> categoryList = new ArrayList<>();
        categoryList.add(0, createMockCategory());
        categoryList.add(1, createMockCategory());
        categoryList.add(2, createMockCategory());

        Mockito.when(mockCategoryRepository.filterByName(name)).thenReturn(categoryList);

        // Act
        List<Category> actualCategories = mockCategoryService.filterByName(name);

        // Assert
        assertEquals(categoryList, actualCategories);
        Mockito.verify(mockCategoryRepository, Mockito.times(1)).filterByName(name);
    }

    @Test
    public void testFindAllByName() {
        //Arrange
        String categoryName = "category";
        Category category1 = createMockCategory();
        Category category2 = createMockCategory();
        List<Category> categoryList = new ArrayList<>();
        categoryList.add(category1);
        categoryList.add(category2);
        Optional<List<Category>> optionalCategoryList = Optional.of(categoryList);
        Mockito.when(mockCategoryRepository.findAllByName(categoryName)).thenReturn(optionalCategoryList);

        //Act
        List<Category> result = mockCategoryService.findAllByName(categoryName);

        // Assert
        assertEquals(2, result.size());
        assertEquals(category1, result.get(0));
        assertEquals(category2, result.get(1));
    }

    @Test
    public void testFindAllByNameNoResult() {
        //Arrange
        String categoryName = "test";
        Optional<List<Category>> optionalCategoryList = Optional.empty();
        Mockito.when(mockCategoryRepository.findAllByName(categoryName)).thenReturn(optionalCategoryList);

        // Act
        List<Category> result = mockCategoryService.findAllByName(categoryName);

        // Assert
        assertTrue(result.isEmpty());
    }
}
