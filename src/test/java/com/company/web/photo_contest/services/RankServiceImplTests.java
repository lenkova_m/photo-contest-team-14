package com.company.web.photo_contest.services;

import com.company.web.photo_contest.Helpers;
import com.company.web.photo_contest.helpers.UserMapper;
import com.company.web.photo_contest.models.Rank;
import com.company.web.photo_contest.modelsDto.RankDto;
import com.company.web.photo_contest.repositories.RankRepository;
import jakarta.persistence.EntityExistsException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class RankServiceImplTests {

    @Mock
    RankRepository rankRepository;

    @InjectMocks
    RankServiceImpl rankService;

    @Mock
    UserMapper userMapper;

    @Test
    void getAll_Should_CallRepository() {

        // Arrange
        when(rankRepository.findAll()).thenReturn(null);

        // Act
        rankService.getAll();

        // Assert
        verify(rankRepository, Mockito.times(1))
                .findAll();
    }

    @Test
    void getRankByName_Should_CallRepository() {
        Rank rank = Helpers.createMockRank();

        // Arrange
        when(rankRepository.findRankByName(rank.getName()))
                .thenReturn(Optional.of(rank));

        // Act
        Rank result = rankService.getRankByName(rank.getName());

        // Assert
        verify(rankRepository, Mockito.times(1))
                .findRankByName(rank.getName());
        assertEquals(rank, result);

    }

    @Test
    void getRankByPoints_Should_CallRepository() {
        Rank rank = Helpers.createMockRank();

        // Arrange
        when(rankRepository.findRankByPoints(rank.getPoints()))
                .thenReturn(rank);

        // Act
        Rank result = rankService.getRankByPoints(rank.getPoints());

        // Assert
        verify(rankRepository, Mockito.times(1))
                .findRankByPoints(rank.getPoints());
        assertEquals(rank, result);

    }

    @Test
    void create_Should_CallRepository_When_ArgumentsAreValid() {
        Rank rank = Helpers.createMockRank();
        RankDto rankDto = new RankDto();
        rankDto.setRankName("Junkie");
        rankDto.setPoints(1);

        // Arrange
        when(userMapper.convertToRankFromCreateDto(rankDto))
                .thenReturn(rank);
        when(rankRepository.findRankByName(rank.getName()))
                .thenReturn(Optional.empty());

        // Act
        rankService.createRank(rankDto);

        // Assert
        verify(rankRepository, Mockito.times(1))
                .save(rank);


    }

    @Test
    void create_Should_Throw_When_RankExists() {
        Rank rank = Helpers.createMockRank();
        RankDto rankDto = new RankDto();
        rankDto.setRankName("test");
        rankDto.setPoints(100);

        // Arrange
        when(userMapper.convertToRankFromCreateDto(rankDto))
                .thenReturn(rank);
        when(rankRepository.findRankByName(rank.getName()))
                .thenReturn(Optional.of(rank));

        // Act, Assert
        assertThrows(EntityExistsException.class,
                () -> rankService.createRank(rankDto));
    }

    @Test
    void update_Should_CallRepository_When_ArgumentsAreValid() {
        Rank rank = Helpers.createMockRank();
        RankDto rankDto = new RankDto();
        rankDto.setRankName("Junkie");
        rankDto.setPoints(1);

        // Arrange
        when(userMapper.convertToRankFromUpdateDto(rank.getRankId(), rankDto))
                .thenReturn(rank);
        when(rankRepository.findRankByName(rank.getName()))
                .thenReturn(Optional.empty());

        // Act
        rankService.updateRank(rank.getRankId(), rankDto);

        // Assert
        verify(rankRepository, Mockito.times(1))
                .save(rank);


    }

    @Test
    void update_Should_Throw_WhenRankExists() {
        // Arrange
        Rank rank = Helpers.createMockRank();
        RankDto rankDto = new RankDto();
        rankDto.setRankName("Junkie");
        rankDto.setPoints(1);
        Rank existingRank = Helpers.createMockRank();
        existingRank.setRankId(2);

        when(userMapper.convertToRankFromUpdateDto(rank.getRankId(), rankDto))
                .thenReturn(rank);
        when(rankRepository.findRankByName(rank.getName()))
                .thenReturn(Optional.of(existingRank));

        // Act, Assert
        assertThrows(EntityExistsException.class,
                () -> rankService.updateRank(rank.getRankId(), rankDto));
    }

    @Test
    void delete_Should_CallRepository() {
        // Arrange
        Rank rank = Helpers.createMockRank();

        // Act
        rankService.deleteRank(rank.getRankId());

        // Assert
        verify(rankRepository, Mockito.times(1))
                .deleteById(rank.getRankId());
    }
}
