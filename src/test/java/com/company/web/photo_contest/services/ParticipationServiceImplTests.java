package com.company.web.photo_contest.services;

import com.company.web.photo_contest.helpers.ParticipationMapper;
import com.company.web.photo_contest.models.*;
import com.company.web.photo_contest.modelsDto.ParticipationDto;
import com.company.web.photo_contest.repositories.*;
import jakarta.persistence.EntityExistsException;
import jakarta.persistence.EntityNotFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.*;

import static com.company.web.photo_contest.Helpers.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)
public class ParticipationServiceImplTests {

    @Mock
    ParticipationRepository mockParticipationRepository;

    @Mock
    ContestRepository mockContestRepository;

    @Mock
    ParticipationMapper participationMapper;

    @Mock
    UserRepository mockUserRepository;

    @Mock
    JurorRepository jurorRepository;

    @Mock
    EvaluationRepository evaluationRepository;

    @InjectMocks
    ParticipationServiceImpl participationService;

    @Test
    void getAll_Should_CallRepository() {
        Mockito.when(mockParticipationRepository.findAll()).thenReturn(null);

        participationService.getAll();

        Mockito.verify(mockParticipationRepository, Mockito.times(1)).findAll();
    }

    @Test
    void getAllByContest_Throw_WhenContestDoesNotExists() {
        Contest mockContest = createMockContest();

        Assertions.assertThrows(EntityNotFoundException.class,
                () -> Optional.ofNullable(participationService.getAllByContest(mockContest.getUuid()))
                        .orElseThrow(EntityNotFoundException::new));
    }

    @Test
    void getAllByContest_Should_CallRepository() {
        Contest mockContest = createMockContest();
        Participation mockParticipation = createMockParticipation();
        mockParticipation.setContest(mockContest);

        Mockito.when(mockParticipationRepository.findAllByContestOrderByAppraisalDesc(mockContest))
                .thenReturn(Collections.singletonList(mockParticipation));

        Mockito.when(mockContestRepository.getContestByUuid(mockContest.getUuid()))
                .thenReturn(Optional.of(mockContest));

        participationService.getAllByContest(mockContest.getUuid());

        Mockito.verify(mockParticipationRepository, Mockito.times(1))
                .findAllByContestOrderByAppraisalDesc(mockContest);
    }

    @Test
    void getAllByUuid_Throw_WhenParticipationDoesNotExists() {
        Participation mockParticipation = createMockParticipation();

        Assertions.assertThrows(EntityNotFoundException.class,
                () -> Optional.ofNullable(participationService.getByUuid(mockParticipation.getUuid()))
                        .orElseThrow(EntityNotFoundException::new));
    }

    @Test
    void getByUuid_Should_CallRepository() {
        Participation mockParticipation = createMockParticipation();

        Mockito.when(mockParticipationRepository.findByUuid(mockParticipation.getUuid()))
                .thenReturn(Optional.of(mockParticipation));

        participationService.getByUuid(mockParticipation.getUuid());

        Mockito.verify(mockParticipationRepository, Mockito.times(1))
                .findByUuid(mockParticipation.getUuid());
    }

    @Test
    void getMappedParticipationAsPage_Should_CallRepository() {
        List<Participation> participations = new ArrayList<>();
        participations.add(new Participation());

        Pageable pageable = PageRequest.of(0, 10);
        Page<Participation> page = new PageImpl<>(participations, pageable, 1);

        List<Evaluation> evaluations = new ArrayList<>();
        evaluations.add(new Evaluation());

        Mockito.when(evaluationRepository.findAllByParticipation(any(Participation.class))).thenReturn(evaluations);
        Mockito.when(participationMapper.viewParticipation(any(Participation.class),
                Mockito.anyList())).thenReturn(new ParticipationDto());

        Page<ParticipationDto> result = participationService.getMappedParticipationAsPage(page);

        assertNotNull(result);
        assertEquals(1, result.getContent().size());
        Mockito.verify(evaluationRepository, Mockito.times(1))
                .findAllByParticipation(any(Participation.class));
        Mockito.verify(participationMapper, Mockito.times(1))
                .viewParticipation(any(Participation.class), Mockito.anyList());
    }

    @Test
    void mapParticipation_Should_CallRepository() {
        UUID contestUuid = UUID.randomUUID();
        Participation mockParticipation = createMockParticipation();
        List<Participation> participations = Arrays.asList(mockParticipation);
        List<Evaluation> evaluationList = Arrays.asList(createMockEvaluation());
        ParticipationDto mockParticipationDto = createMockParticipationDto();
        Contest mockContest = createMockContest();

        Mockito.when(mockContestRepository.getContestByUuid(contestUuid)).thenReturn(Optional.of(mockContest));
        Mockito.when(participationService.getAllByContest(contestUuid)).thenReturn(participations);
        Mockito.when(evaluationRepository.findAllByParticipation(any(Participation.class))).thenReturn(evaluationList);
        Mockito.when(participationMapper.viewParticipation(mockParticipation, evaluationList)).thenReturn(mockParticipationDto);

        List<ParticipationDto> result = participationService.mapParticipation(contestUuid);

        assertEquals(1, result.size());
        assertSame(mockParticipationDto, result.get(0));
    }

    @Test
    void getByTitle_Should_CallRepository() {
        Participation mockParticipation = createMockParticipation();

        Mockito.when(mockParticipationRepository.findParticipationByTitle(Mockito.anyString()))
                .thenReturn(Optional.of(Collections.singletonList(mockParticipation)));

        participationService.getByTitle(Mockito.anyString());

        Mockito.verify(mockParticipationRepository, Mockito.times(1))
                .findParticipationByTitle(Mockito.anyString());
    }

    @Test
    void getAllWinners_Should_CallRepository() {

        participationService.getAllWinners(0, 10);

        Mockito.verify(mockParticipationRepository, Mockito.times(1))
                .findAllWinners(any(Pageable.class));
    }

    @Test
    void getAllParticipationsForContest_Should_CallRepository() {
        UUID contestUuid = UUID.randomUUID();
        List<Participation> participations = new ArrayList<>();

        Mockito.when(mockParticipationRepository.findAllByContestUuid(contestUuid))
                .thenReturn(Optional.of(participations));

        participationService.getAllParticipationsForContest(contestUuid);

        Mockito.verify(mockParticipationRepository, Mockito.times(1))
                .findAllByContestUuid(contestUuid);
    }

    @Test
    void getAllParticipationsForContest_Throw_WhenParticipationsNotFound() {
        Contest mockContest = createMockContest();

        Mockito.when(mockParticipationRepository.findAllByContestUuid(mockContest.getUuid()))
                .thenReturn(Optional.empty());

        Assertions.assertThrows(EntityNotFoundException.class,
                () -> participationService.getAllParticipationsForContest(mockContest.getUuid()));
    }

    @Test
    void getByTitle_Throw_WhenParticipationNotExists() {
        Mockito.when(mockParticipationRepository.findParticipationByTitle(Mockito.anyString()))
                .thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class,
                () -> participationService.getByTitle(Mockito.anyString()));
    }

    @Test
    void getAllParticipationsForContestAsPage_Should_CallRepository() {
        Contest mockContest = createMockContest();

        Mockito.when(mockContestRepository.getContestByUuid(mockContest.getUuid()))
                .thenReturn(Optional.of(mockContest));

        Pageable pageable = PageRequest.of(0, 10);

        participationService.getAllParticipationsForContestAsPage(0, 10, mockContest.getUuid());

        Mockito.verify(mockParticipationRepository, Mockito.times(1))
                .findAllByContestOrderByDateCreatedDesc(mockContest, pageable);
    }


    @Test
    void getAllParticipationsForContestAsPage_Throw_WhenContestNotFound() {

        Contest mockContest = createMockContest();

        Mockito.when(mockContestRepository.getContestByUuid(mockContest.getUuid()))
                .thenReturn(Optional.empty());

        Assertions.assertThrows(EntityNotFoundException.class,
                () -> participationService.getAllParticipationsForContestAsPage(0, 10, mockContest.getUuid()));
    }

    @Test
    void getByPlacement_Should_CallRepository() {
        Participation mockParticipation = createMockParticipation();

        Mockito.when(mockParticipationRepository.findParticipationByPlacement(Mockito.anyInt()))
                .thenReturn(Optional.of(Collections.singletonList(mockParticipation)));

        participationService.getByPlacement(Mockito.anyInt());

        Mockito.verify(mockParticipationRepository, Mockito.times(1))
                .findParticipationByPlacement(Mockito.anyInt());
    }

    @Test
    void getByPlacement_Throw_WhenParticipationNotExists() {
        Mockito.when(mockParticipationRepository.findParticipationByPlacement(Mockito.anyInt()))
                .thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class,
                () -> participationService.getByPlacement(Mockito.anyInt()));
    }

    @Test
    void getWinners_Should_CallRepository() {
        Participation mockParticipation = createMockParticipation();
        Contest mockContest = createMockContest();
        mockContest.setFinished(true);

        Mockito.when(mockParticipationRepository.findWinnersFromAllFinishedContestsLastSix())
                .thenReturn(Collections.singletonList(mockParticipation));

        participationService.getWinners();

        Mockito.verify(mockParticipationRepository, Mockito.times(1))
                .findWinnersFromAllFinishedContestsLastSix();
    }

    @Test
    void create_Should_CallRepository() {
        ParticipationDto mockParticipationDto = createMockParticipationDto();
        Contest mockContest = createMockContest();
        Participation mockParticipation = createMockParticipation();
        User mockUser = createMockUser();
        mockParticipation.setUser(mockUser);
        mockParticipationDto.setUser(mockUser);


        Mockito.when(mockParticipationRepository.findParticipationByUserAndContest(mockUser, mockContest))
                .thenReturn(Optional.empty());

        Mockito.when(mockUserRepository.findByUsername(mockUser.getUsername()))
                .thenReturn(Optional.of(mockParticipationDto.getUser()));

        Mockito.when(mockContestRepository.getContestByUuid(mockParticipationDto.getContestUuid()))
                .thenReturn(Optional.of(mockContest));

        Mockito.when(participationMapper.fromDto(mockContest, mockParticipationDto, mockUser))
                .thenReturn(mockParticipation);

        Mockito.when(jurorRepository.getJurorByContestIdAndUserId(Mockito.anyInt(), Mockito.anyInt()))
                .thenReturn(Optional.empty());

        participationService.create(mockParticipationDto, mockUser.getUsername());

        Mockito.verify(mockParticipationRepository, Mockito.times(1))
                .save(mockParticipation);
    }

    @Test
    void create_Throw_WhenUserIsJuror() {
        ParticipationDto mockParticipationDto = createMockParticipationDto();
        Contest mockContest = createMockContest();
        Participation mockParticipation = createMockParticipation();
        User mockUser = createMockUser();
        mockParticipation.setUser(mockUser);
        mockParticipationDto.setUser(mockUser);
        Juror mockJuror = createMockJuror();


        Mockito.when(mockParticipationRepository.findParticipationByUserAndContest(mockUser, mockContest))
                .thenReturn(Optional.empty());

        Mockito.when(mockUserRepository.findByUsername(mockUser.getUsername()))
                .thenReturn(Optional.of(mockParticipationDto.getUser()));

        Mockito.when(mockContestRepository.getContestByUuid(mockParticipationDto.getContestUuid()))
                .thenReturn(Optional.of(mockContest));

        Mockito.when(participationMapper.fromDto(mockContest, mockParticipationDto, mockUser))
                .thenReturn(mockParticipation);

        Mockito.when(jurorRepository.getJurorByContestIdAndUserId(Mockito.anyInt(), Mockito.anyInt()))
                .thenReturn(Optional.of(mockJuror));

        Assertions.assertThrows(EntityExistsException.class,
                () -> participationService.create(mockParticipationDto, mockUser.getUsername()));
    }

    @Test
    void create_Throw_WhenUserIsAlreadyParticipating() {
        ParticipationDto mockParticipationDto = createMockParticipationDto();
        Contest mockContest = createMockContest();
        Participation mockParticipation = createMockParticipation();
        User mockUser = createMockUser();
        mockParticipation.setUser(mockUser);
        mockParticipationDto.setUser(mockUser);


        Mockito.when(mockParticipationRepository.findParticipationByUserAndContest(mockUser, mockContest))
                .thenReturn(Optional.of(mockParticipation));

        Mockito.when(mockUserRepository.findByUsername(mockUser.getUsername()))
                .thenReturn(Optional.of(mockParticipationDto.getUser()));

        Mockito.when(mockContestRepository.getContestByUuid(mockParticipationDto.getContestUuid()))
                .thenReturn(Optional.of(mockContest));

        Mockito.when(participationMapper.fromDto(mockContest, mockParticipationDto, mockUser))
                .thenReturn(mockParticipation);

        Assertions.assertThrows(EntityExistsException.class,
                () -> participationService.create(mockParticipationDto, mockUser.getUsername()));
    }

    @Test
    void delete_Should_CallRepository() {
        Participation mockParticipation = createMockParticipation();
        User mockUser = createMockUser();
        mockParticipation.setUser(mockUser);
        Juror mockJuror = createMockJuror();


        Mockito.when(mockParticipationRepository.findByUuid(mockParticipation.getUuid()))
                .thenReturn(Optional.of(mockParticipation));

        Mockito.when(mockUserRepository.findByUsername(Mockito.anyString()))
                .thenReturn(Optional.of(mockParticipation.getUser()));

        Mockito.when(jurorRepository.getJurorByContestIdAndUserId(Mockito.anyInt(), Mockito.anyInt()))
                .thenReturn(Optional.of(mockJuror));

        participationService.delete(mockParticipation.getUuid(), mockUser.getUsername());

        Mockito.verify(mockParticipationRepository, Mockito.times(1))
                .delete(mockParticipation);
    }

    @Test
    void delete_Throw_WhenParticipationNotExists() {
        Participation mockParticipation = createMockParticipation();
        User mockUser = createMockUser();
        mockParticipation.setUser(mockUser);


        Mockito.when(mockParticipationRepository.findByUuid(mockParticipation.getUuid()))
                .thenReturn(Optional.empty());

        Assertions.assertThrows(EntityNotFoundException.class,
                () -> participationService.delete(mockParticipation.getUuid(), mockUser.getUsername()));

    }

    @Test
    void delete_Throw_WhenUserNotExists() {
        Participation mockParticipation = createMockParticipation();
        User mockUser = createMockUser();
        mockParticipation.setUser(mockUser);


        Mockito.when(mockParticipationRepository.findByUuid(mockParticipation.getUuid()))
                .thenReturn(Optional.of(mockParticipation));

        Mockito.when(mockUserRepository.findByUsername(Mockito.anyString()))
                .thenReturn(Optional.empty());

        Assertions.assertThrows(EntityNotFoundException.class,
                () -> participationService.delete(mockParticipation.getUuid(), mockUser.getUsername()));

    }

    @Test
    void delete_Throw_WhenJurorNotExists() {
        Participation mockParticipation = createMockParticipation();
        User mockUser = createMockUser();
        mockParticipation.setUser(mockUser);


        Mockito.when(mockParticipationRepository.findByUuid(mockParticipation.getUuid()))
                .thenReturn(Optional.of(mockParticipation));

        Mockito.when(mockUserRepository.findByUsername(Mockito.anyString()))
                .thenReturn(Optional.of(mockParticipation.getUser()));

        Mockito.when(jurorRepository.getJurorByContestIdAndUserId(Mockito.anyInt(), Mockito.anyInt()))
                .thenReturn(Optional.empty());

        Assertions.assertThrows(EntityNotFoundException.class,
                () -> participationService.delete(mockParticipation.getUuid(), mockUser.getUsername()));
    }
}
