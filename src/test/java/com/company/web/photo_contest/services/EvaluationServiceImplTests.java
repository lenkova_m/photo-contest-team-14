package com.company.web.photo_contest.services;

import com.company.web.photo_contest.helpers.EvaluationMapper;
import com.company.web.photo_contest.models.*;
import com.company.web.photo_contest.modelsDto.EvaluationDto;
import com.company.web.photo_contest.modelsDto.EvaluationResponseDto;
import com.company.web.photo_contest.repositories.EvaluationRepository;
import com.company.web.photo_contest.repositories.JurorRepository;
import com.company.web.photo_contest.repositories.ParticipationRepository;
import com.company.web.photo_contest.repositories.UserRepository;
import jakarta.persistence.EntityExistsException;
import jakarta.persistence.EntityNotFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static com.company.web.photo_contest.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class EvaluationServiceImplTests {

    @Mock
    ParticipationRepository mockParticipationRepository;

    @Mock
    EvaluationMapper evaluationMapper;

    @Mock
    UserRepository mockUserRepository;

    @Mock
    JurorRepository jurorRepository;

    @Mock
    EvaluationRepository evaluationRepository;

    @InjectMocks
    EvaluationServiceImpl evaluationService;

    @Test
    void getAll_Should_CallRepository() {
        Mockito.when(evaluationRepository.findAll()).thenReturn(null);

        evaluationService.getAll();

        Mockito.verify(evaluationRepository, Mockito.times(1)).findAll();
    }

    @Test
    void getAllByParticipation_Should_CallRepository() {
        Participation mockParticipation = createMockParticipation();
        Evaluation mockEvaluation = createMockEvaluation();
        EvaluationResponseDto evaluationResponseDto = createMockEvaluationResponseDto();
        List<Evaluation> evaluationList = new ArrayList<>();
        evaluationList.add(mockEvaluation);
        List<EvaluationResponseDto> mappedList = new ArrayList<>();
        Juror mockJuror = createMockJuror();
        User mockUser = createMockUser();
        List<EvaluationResponseDto> list = new ArrayList<>();
        list.add(evaluationResponseDto);

        Mockito.when(mockParticipationRepository.findByUuid(mockParticipation.getUuid()))
                .thenReturn(Optional.of(mockParticipation));

        Mockito.when(evaluationRepository.findAllByParticipation(mockParticipation))
                .thenReturn(evaluationList);

        Mockito.when(mockUserRepository.getReferenceById(mockJuror.getUserId()))
                .thenReturn(mockUser);

        Mockito.when(evaluationMapper.convertToDto(mockParticipation, mockEvaluation, mockUser))
                .thenReturn(evaluationResponseDto);

        mappedList.add(evaluationResponseDto);

        evaluationService.getAllEvaluationDtoByParticipation(mockParticipation.getUuid());

        Assertions.assertEquals(mappedList, list);
    }

    @Test
    void getAllByParticipation_Throw_WhenParticipationNotExists() {
        Participation mockParticipation = createMockParticipation();

        Mockito.when(mockParticipationRepository.findByUuid(mockParticipation.getUuid()))
                .thenReturn(Optional.empty());

        Assertions.assertThrows(EntityNotFoundException.class,
                () -> evaluationService.getAllEvaluationDtoByParticipation(mockParticipation.getUuid()));
    }

    @Test
    void getById_Should_CallRepository() {
        Evaluation mockEvaluation = createMockEvaluation();

        Mockito.when(evaluationRepository.findById(Mockito.anyInt()))
                .thenReturn(Optional.of(mockEvaluation));

        evaluationService.getById(Mockito.anyInt());

        Mockito.verify(evaluationRepository, Mockito.times(1))
                .findById(Mockito.anyInt());
    }

    @Test
    void getById_Throw_WhenEvaluationNotExists() {
        Mockito.when(evaluationRepository.findById(Mockito.anyInt()))
                .thenReturn(Optional.empty());

        Assertions.assertThrows(EntityNotFoundException.class,
                () -> evaluationService.getById(Mockito.anyInt()));
    }

    @Test
    void create_Should_CallRepository() {
        Participation mockParticipation = createMockParticipation();
        EvaluationDto mockEvaluationDto = createMockEvaluationDto();
        Juror mockJuror = createMockJuror();
        User mockUser = createMockUser();
        Evaluation mockEvaluation = createMockEvaluation();

        Mockito.when(mockParticipationRepository.findByUuid(mockEvaluationDto.getParticipationUuid()))
                .thenReturn(Optional.of(mockParticipation));

        Mockito.when(mockUserRepository.findByUsername(Mockito.anyString()))
                .thenReturn(Optional.of(mockUser));

        Mockito.when(jurorRepository.getJurorByContestIdAndUserId(Mockito.anyInt(), Mockito.anyInt()))
                .thenReturn(Optional.of(mockJuror));

        Mockito.when(evaluationMapper.fromDto(mockParticipation, mockEvaluationDto, mockJuror))
                .thenReturn(mockEvaluation);

        Mockito.when(evaluationRepository.findByJurorAndParticipation(mockEvaluation.getJuror(),
                        mockEvaluation.getParticipation()))
                .thenReturn(Optional.empty());

        evaluationService.create(mockEvaluationDto, Mockito.anyString());

        Mockito.verify(evaluationRepository, Mockito.times(1))
                .save(mockEvaluation);
    }

    @Test
    void create_Throw_WhenParticipationNotExists() {
        EvaluationDto mockEvaluationDto = createMockEvaluationDto();

        Mockito.when(mockParticipationRepository.findByUuid(mockEvaluationDto.getParticipationUuid()))
                .thenReturn(Optional.empty());

        Assertions.assertThrows(EntityNotFoundException.class,
                () -> evaluationService.create(mockEvaluationDto, Mockito.anyString()));
    }

    @Test
    void create_Throw_WhenUserNotExists() {
        Participation mockParticipation = createMockParticipation();
        EvaluationDto mockEvaluationDto = createMockEvaluationDto();

        Mockito.when(mockParticipationRepository.findByUuid(mockEvaluationDto.getParticipationUuid()))
                .thenReturn(Optional.of(mockParticipation));

        Mockito.when(mockUserRepository.findByUsername(Mockito.anyString()))
                .thenReturn(Optional.empty());

        Assertions.assertThrows(EntityNotFoundException.class,
                () -> evaluationService.create(mockEvaluationDto, Mockito.anyString()));
    }

    @Test
    void create_Throw_WhenJurorNotExists() {
        Participation mockParticipation = createMockParticipation();
        EvaluationDto mockEvaluationDto = createMockEvaluationDto();
        User mockUser = createMockUser();

        Mockito.when(mockParticipationRepository.findByUuid(mockEvaluationDto.getParticipationUuid()))
                .thenReturn(Optional.of(mockParticipation));

        Mockito.when(mockUserRepository.findByUsername(Mockito.anyString()))
                .thenReturn(Optional.of(mockUser));

        Mockito.when(jurorRepository.getJurorByContestIdAndUserId(Mockito.anyInt(), Mockito.anyInt()))
                .thenReturn(Optional.empty());

        Assertions.assertThrows(EntityNotFoundException.class,
                () -> evaluationService.create(mockEvaluationDto, Mockito.anyString()));
    }

    @Test
    void create_Throw_WhenEvaluationExists() {
        Participation mockParticipation = createMockParticipation();
        EvaluationDto mockEvaluationDto = createMockEvaluationDto();
        Juror mockJuror = createMockJuror();
        User mockUser = createMockUser();
        Evaluation mockEvaluation = createMockEvaluation();

        Mockito.when(mockParticipationRepository.findByUuid(mockEvaluationDto.getParticipationUuid()))
                .thenReturn(Optional.of(mockParticipation));

        Mockito.when(mockUserRepository.findByUsername(Mockito.anyString()))
                .thenReturn(Optional.of(mockUser));

        Mockito.when(jurorRepository.getJurorByContestIdAndUserId(Mockito.anyInt(), Mockito.anyInt()))
                .thenReturn(Optional.of(mockJuror));

        Mockito.when(evaluationMapper.fromDto(mockParticipation, mockEvaluationDto, mockJuror))
                .thenReturn(mockEvaluation);

        Mockito.when(evaluationRepository.findByJurorAndParticipation(mockEvaluation.getJuror(), mockEvaluation.getParticipation()))
                .thenReturn(Optional.of(mockEvaluation));

        Assertions.assertThrows(EntityExistsException.class,
                () -> evaluationService.create(mockEvaluationDto, Mockito.anyString()));
    }

    @Test
    void createDefaultEvaluation_Should_NotCallUpdateDefaultParticipationAppraisal() {
        Participation mockParticipation = createMockParticipation();
        List<Participation> participations = Arrays.asList(mockParticipation);
        List<Evaluation> evaluationList = Arrays.asList(createMockEvaluation());

        Mockito.when(mockParticipationRepository.findByUuid(mockParticipation.getUuid()))
                .thenReturn(Optional.of(mockParticipation));

        mockParticipation.getContest().setFinished(true);

        evaluationService.createDefaultEvaluation(mockParticipation.getUuid());

        Assertions.assertEquals(participations.size(), evaluationList.size());
    }

    @Test
    void updateDefaultParticipationAppraisal_Shoud_ReturnCorrectValue() {
        Participation participation = createMockParticipation();
        participation.setAppraisal(0.0);

        Evaluation evaluation1 = createMockEvaluation();
        evaluation1.setParticipation(participation);
        evaluation1.setRating(5);

        Evaluation evaluation2 = createMockEvaluation();
        evaluation2.setEvaluationId(2);
        evaluation2.setParticipation(participation);
        evaluation2.setRating(4);

        List<Evaluation> evaluations = new ArrayList<>();
        evaluations.add(evaluation1);
        evaluations.add(evaluation2);

        Mockito.when(evaluationRepository.findAllByParticipation(participation)).thenReturn(evaluations);

        evaluationService.updateDefaultParticipationAppraisal(participation, 1);

        Assertions.assertEquals(4.00, participation.getAppraisal(), 0.01);
    }

    @Test
    void updateParticipationPlacement_Should_ReturnCorrectValues() {
        Contest testContest = createMockContest();

        Participation mockParticipation1 = createMockParticipation();
        mockParticipation1.setParticipationId(1);
        mockParticipation1.setAppraisal(4.5);

        Participation mockParticipation2 = createMockParticipation();
        mockParticipation2.setParticipationId(2);
        mockParticipation2.setAppraisal(3.5);

        Participation mockParticipation3 = createMockParticipation();
        mockParticipation3.setParticipationId(3);
        mockParticipation3.setAppraisal(4.0);

        List<Participation> testParticipationList = new ArrayList<>();
        testParticipationList.add(mockParticipation1);
        testParticipationList.add(mockParticipation2);
        testParticipationList.add(mockParticipation3);

        Mockito.when(mockParticipationRepository.findAllByContestOrderByAppraisalDesc(testContest))
                .thenReturn(testParticipationList);

        evaluationService.updateParticipationPlacement(testContest);

        Assertions.assertEquals(1, testParticipationList.get(0).getPlacement());
        Assertions.assertEquals(2, testParticipationList.get(1).getPlacement());
        Assertions.assertEquals(3, testParticipationList.get(2).getPlacement());
    }

    @Test
    void updateParticipationAppraisal_Should_ReturnCorrectValues() {
        Evaluation mockEvaluation1 = createMockEvaluation();
        mockEvaluation1.setEvaluationId(1);
        mockEvaluation1.setRating(5);

        Evaluation mockEvaluation2 = createMockEvaluation();
        mockEvaluation2.setEvaluationId(2);
        mockEvaluation2.setRating(4);

        Evaluation mockEvaluation3 = createMockEvaluation();
        mockEvaluation3.setEvaluationId(3);
        mockEvaluation3.setRating(3);

        List<Evaluation> evaluations = new ArrayList<>();
        evaluations.add(mockEvaluation1);
        evaluations.add(mockEvaluation2);
        evaluations.add(mockEvaluation3);

        Participation mockParticipation = createMockParticipation();

        Mockito.when(evaluationRepository.findAllByParticipation(mockParticipation))
                .thenReturn(evaluations);

        evaluationService.updateParticipationAppraisal(mockParticipation);

        Assertions.assertEquals(4.0, mockParticipation.getAppraisal(), 0.01);
    }
}
