package com.company.web.photo_contest.services;

import com.company.web.photo_contest.Helpers;
import com.company.web.photo_contest.helpers.JavaMailSenderHelper;
import com.company.web.photo_contest.helpers.UserMapper;
import com.company.web.photo_contest.models.*;
import com.company.web.photo_contest.modelsDto.UserRegistrationRequest;
import com.company.web.photo_contest.modelsDto.UserUpdateRequestDto;
import com.company.web.photo_contest.repositories.ParticipationRepository;
import com.company.web.photo_contest.repositories.RankRepository;
import com.company.web.photo_contest.repositories.RoleRepository;
import com.company.web.photo_contest.repositories.UserRepository;
import jakarta.mail.MessagingException;
import jakarta.persistence.EntityExistsException;
import jakarta.persistence.EntityNotFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UserServiceImplTests {

    @Mock
    UserRepository userRepository;
    @InjectMocks
    UserServiceImpl mockUserService;
    @Mock
    RoleRepository roleRepository;
    @Mock
    RankRepository rankRepository;
    @Mock
    ParticipationRepository participationRepository;
    @Mock
    UserMapper userMapper;
    @Mock
    JavaMailSenderHelper mailSenderHelper;
    @Mock
    BCryptPasswordEncoder passwordEncoder;

    @Test
    void getAll_Should_CallRepository() {

        // Arrange
        when(userRepository.findAll()).thenReturn(null);

        // Act
        mockUserService.getAll();

        // Assert
        verify(userRepository, Mockito.times(1))
                .findAll();
    }

    @Test
    void getAllByRole_Should_CallRepository() {
        User mockUser = Helpers.createMockUser();
        List<User> mockUserList = new ArrayList<>();
        mockUserList.add(mockUser);

        // Arrange
        when(userRepository.findUserByRoleName(anyString()))
                .thenReturn(List.of(mockUser));

        // Act
        List<User> result = mockUserService.getAllByRole(mockUser.getRole().getName());

        // Assert
        verify(userRepository, Mockito.times(1))
                .findUserByRoleName(mockUser.getRole().getName());
        assertEquals(mockUserList, result);

    }

    @Test
    void getByUuid_Should_CallRepository() {
        User mockUser = Helpers.createMockUser();

        // Arrange
        when(userRepository.findByUuid(mockUser.getUuid()))
                .thenReturn(Optional.of(mockUser));

        // Act
        User result = mockUserService.getUserByUuid(mockUser.getUuid());

        // Assert
        verify(userRepository, Mockito.times(1))
                .findByUuid(mockUser.getUuid());
        assertEquals(mockUser, result);

    }

    @Test
    void getByUsername_Should_CallRepository() {
        User mockUser = Helpers.createMockUser();

        // Arrange
        when(userRepository.findByUsername(mockUser.getUsername()))
                .thenReturn(Optional.of(mockUser));

        // Act
        User result = mockUserService.getUserByUsername(mockUser.getUsername());

        // Assert
        verify(userRepository, Mockito.times(1))
                .findByUsername(mockUser.getUsername());
        assertEquals(mockUser, result);

    }

    @Test
    void getByEmail_Should_CallRepository() {
        User mockUser = Helpers.createMockUser();

        // Arrange
        when(userRepository.findByEmail(mockUser.getEmail()))
                .thenReturn(Optional.of(mockUser));

        // Act
        User result = mockUserService.getUserByEmail(mockUser.getEmail());

        // Assert
        verify(userRepository, Mockito.times(1))
                .findByEmail(mockUser.getEmail());
        assertEquals(mockUser, result);

    }

    @Test
    void getByPasswordResetToken_Should_CallRepository() {
        User mockUser = Helpers.createMockUser();
        String token = UUID.randomUUID().toString();
        mockUser.setPasswordResetToken(token);

        // Arrange
        when(userRepository.findByPasswordResetToken(token))
                .thenReturn(Optional.of(mockUser));

        // Act
        User result = mockUserService.getByPasswordResetToken(token);

        // Assert
        verify(userRepository, Mockito.times(1))
                .findByPasswordResetToken(token);
        assertEquals(mockUser, result);
    }

    @Test
    void getByEmailConfirmationToken_Should_CallRepository() {
        User mockUser = Helpers.createMockUser();
        String token = UUID.randomUUID().toString();
        mockUser.setPasswordResetToken(token);

        // Arrange
        when(userRepository.findByEmailConfirmationToken(token))
                .thenReturn(Optional.of(mockUser));

        // Act
        User result = mockUserService.getByEmailConfirmationToken(token);

        // Assert
        verify(userRepository, Mockito.times(1))
                .findByEmailConfirmationToken(token);
        assertEquals(mockUser, result);
    }

    @Test
    void findUsersByRank_Should_CallRepository() {
        User mockUser = Helpers.createMockUser();
        List<User> mockUserList = new ArrayList<>();
        mockUserList.add(mockUser);

        // Arrange
        when(userRepository.findUsersByRank())
                .thenReturn(List.of(mockUser));

        // Act
        List<User> result = mockUserService.findUsersByRank();

        // Assert
        verify(userRepository, Mockito.times(1))
                .findUsersByRank();
        assertEquals(mockUserList, result);

    }

    @Test
    public void findAllUsersByRole_Should_ReturnsEmptyPage() {
        Pageable pageable = PageRequest.of(0, 10);
        Page<User> emptyPage = Page.empty();

        Mockito.when(userRepository.findAllUsersWithRolePhotoJunkies(pageable))
                .thenReturn(emptyPage);

        Page<User> page = mockUserService.findAllUsersByRole(0, 10);

        assertTrue(page.isEmpty());
    }

    @Test
    void findBy_Should_CallRepository() {
        User mockUser = Helpers.createMockUser();
        User mockUser2 = Helpers.createMockUser();
        mockUser2.setUsername("test");
        List<User> mockUsers = new ArrayList<>();
        mockUsers.add(mockUser2);

        // Arrange
        when(userRepository.
                findByFirstNameContainingIgnoreCaseAndLastNameContainingIgnoreCaseAndUsernameNotContainingIgnoreCase(
                        "", "", mockUser2.getUsername()))
                .thenReturn(List.of(mockUser2));

        // Act
        List<User> result = mockUserService.findBy("", "", mockUser2.getUsername());

        // Assert
        verify(userRepository, Mockito.times(1))
                .findByFirstNameContainingIgnoreCaseAndLastNameContainingIgnoreCaseAndUsernameNotContainingIgnoreCase(
                        "", "", mockUser2.getUsername());
        assertEquals(mockUsers, result);
    }

    @Test
    void create_Should_CallRepository_When_ArgumentsAreValid() {
        User mockUser = Helpers.createMockUser();
        UserRegistrationRequest mockRequest = Helpers.createUserRegistrationRequest();

        // Arrange
        when(roleRepository.findRoleByName(mockUser.getRole().getName()))
                .thenReturn(Optional.ofNullable(mockUser.getRole()));
        when(rankRepository.findRankByName(mockUser.getRank().getName()))
                .thenReturn(Optional.ofNullable(mockUser.getRank()));
        when(userMapper.convertToUserFromRegistrationDto(mockRequest, mockUser.getRole(), mockUser.getRank()))
                .thenReturn(mockUser);
        when(userRepository.findByUsername(mockUser.getUsername()))
                .thenReturn(Optional.empty());
        when(userRepository.save(mockUser))
                .thenReturn(mockUser);

        // Act
        User result = mockUserService.createUser(mockRequest);

        // Assert
        verify(userRepository, Mockito.times(1))
                .save(mockUser);
        assertEquals(mockUser, result);
    }

    @Test
    void create_Should_Throw_When_RoleDoesNotExist() {
        UserRegistrationRequest mockRequest = Helpers.createUserRegistrationRequest();

        // Arrange
        when(roleRepository.findRoleByName(anyString()))
                .thenThrow(EntityNotFoundException.class);

        // Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> mockUserService.createUser(mockRequest));

    }

    @Test
    void create_Should_Throw_When_RankDoesNotExist() {
        User mockUser = Helpers.createMockUser();
        UserRegistrationRequest mockRequest = Helpers.createUserRegistrationRequest();

        // Arrange
        when(roleRepository.findRoleByName(anyString()))
                .thenReturn(Optional.ofNullable(mockUser.getRole()));
        when(rankRepository.findRankByName(anyString()))
                .thenThrow(EntityNotFoundException.class);

        // Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> mockUserService.createUser(mockRequest));

    }

    @Test
    void create_Should_Throw_When_UsernameExists() {
        User mockUser = Helpers.createMockUser();
        UserRegistrationRequest mockRequest = Helpers.createUserRegistrationRequest();

        // Arrange
        when(roleRepository.findRoleByName(anyString()))
                .thenReturn(Optional.ofNullable(mockUser.getRole()));
        when(rankRepository.findRankByName(anyString()))
                .thenReturn(Optional.ofNullable(mockUser.getRank()));
        when(userMapper.convertToUserFromRegistrationDto(mockRequest, mockUser.getRole(), mockUser.getRank()))
                .thenReturn(mockUser);
        when(userRepository.findByUsername(mockUser.getUsername()))
                .thenReturn(Optional.of(mockUser));

        // Act, Assert
        Assertions.assertThrows(EntityExistsException.class,
                () -> mockUserService.createUser(mockRequest));

    }

    @Test
    void verifyEmail_Should_Throw_When_TokenDoesNotBelongToAnyUser() {
        String token = UUID.randomUUID().toString();

        // Arrange
        when(userRepository.findByEmailConfirmationToken(token))
                .thenThrow(EntityNotFoundException.class);

        // Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> mockUserService.verifyEmail(token));
    }

    @Test
    void verifyEmail_Should_CallRepository_When_TokenBelongsToUser() {
        // Arrange
        User mockUser = Helpers.createMockUser();
        String token = UUID.randomUUID().toString();
        mockUser.setEmailConfirmationToken(token);

        when(userRepository.findByEmailConfirmationToken(token))
                .thenReturn(Optional.of(mockUser));
        // Act
        mockUserService.verifyEmail(token);

        // Assert
        verify(userRepository, Mockito.times(1))
                .save(mockUser);
    }

    @Test
    void verifyEmail_ReturnFalse_When_UserEmailIsEnabled() {
        // Arrange
        User mockUser = Helpers.createMockUser();
        String token = UUID.randomUUID().toString();
        mockUser.setEmailConfirmationToken(token);
        mockUser.setEnabled(true);

        when(userRepository.findByEmailConfirmationToken(token))
                .thenReturn(Optional.of(mockUser));
        // Act
        boolean isEmailVerificationNeeded = mockUserService.verifyEmail(token);

        // Assert
        assertFalse(isEmailVerificationNeeded);
    }

    @Test
    void checkEmail_Should_Throw_When_EmailExists() {
        User mockUser = Helpers.createMockUser();
        UserRegistrationRequest mockRequest = Helpers.createUserRegistrationRequest();
        String siteUrl = "www.test.com";

        // Arrange
        when(userRepository.findByEmail(mockUser.getEmail()))
                .thenReturn(Optional.of(mockUser));

        // Act, Assert
        Assertions.assertThrows(EntityExistsException.class,
                () -> mockUserService.checkEmail(mockRequest, siteUrl));

    }

    @Test
    void checkEmail_Should_CallCreateUser_When_ArgumentsEmailDoesNotExist()
            throws MessagingException, UnsupportedEncodingException {
        User mockUser = Helpers.createMockUser();
        UserRegistrationRequest mockRequest = Helpers.createUserRegistrationRequest();
        String siteUrl = "www.test.com";

        // Arrange
        when(userRepository.findByEmail(mockUser.getEmail()))
                .thenReturn(Optional.empty());
        when(roleRepository.findRoleByName(mockUser.getRole().getName()))
                .thenReturn(Optional.ofNullable(mockUser.getRole()));
        when(rankRepository.findRankByName(mockUser.getRank().getName()))
                .thenReturn(Optional.ofNullable(mockUser.getRank()));
        when(userMapper.convertToUserFromRegistrationDto(mockRequest, mockUser.getRole(), mockUser.getRank()))
                .thenReturn(mockUser);
        when(userRepository.findByUsername(mockUser.getUsername()))
                .thenReturn(Optional.empty());
        when(userRepository.save(mockUser))
                .thenReturn(mockUser);

        // Act
        mockUserService.checkEmail(mockRequest, siteUrl);

        // Assert
        verify(userRepository, times(2)).save(mockUser);
    }

    @Test
    void updateUser_Should_CallRepository() {
        User mockUser = Helpers.createMockUser();
        UserUpdateRequestDto mockRequest = Helpers.createUserUpdateRequest();

        // Arrange
        when(userRepository.findByUuid(mockUser.getUuid()))
                .thenReturn(Optional.of(mockUser));
        when(userRepository.findByUsername(anyString()))
                .thenReturn(Optional.of(mockUser));
        when(userMapper.convertToUserFromUpdateDto(mockUser, mockRequest))
                .thenReturn(mockUser);
        when(userRepository.save(mockUser))
                .thenReturn(mockUser);

        // Act
        mockUserService.updateUser(mockUser.getUuid(), mockRequest, mockUser.getUsername());

        // Assert
        verify(userRepository, times(1)).save(mockUser);
    }

    @Test
    void updateUser_Should_Throw_When_UserIsNotCreator() {
        User mockUser = Helpers.createMockUser();
        User mockUser2 = Helpers.createMockUser();
        mockUser2.setUuid(UUID.randomUUID());
        UserUpdateRequestDto mockRequest = Helpers.createUserUpdateRequest();

        // Arrange
        when(userRepository.findByUuid(mockUser.getUuid()))
                .thenReturn(Optional.of(mockUser));
        when(userRepository.findByUsername(anyString()))
                .thenReturn(Optional.of(mockUser2));

        // Act, Assert
        Assertions.assertThrows(AccessDeniedException.class,
                () -> mockUserService.updateUser(mockUser.getUuid(), mockRequest, mockUser.getUsername()));
    }

    @Test
    void updateUser_Should_Throw_When_UserDoesNotExist() {
        User mockUser = Helpers.createMockUser();
        UserUpdateRequestDto mockRequest = Helpers.createUserUpdateRequest();

        // Arrange
        when(userRepository.findByUuid(mockUser.getUuid()))
                .thenReturn(Optional.empty());

        // Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> mockUserService.updateUser(mockUser.getUuid(), mockRequest, mockUser.getUsername()));
    }

    @Test
    void deleteUser_Should_CallRepository() {
        // Arrange
        User mockUser = Helpers.createMockUser();

        // Act
        mockUserService.deleteUser(mockUser.getUuid());

        // Assert
        verify(userRepository, times(1))
                .deleteByUuid(mockUser.getUuid());
    }

    @Test
    void makeOrganizer_Should_CallRepository() {
        // Arrange
        User mockUser = Helpers.createMockUser();
        Role role = new Role("Organizer");

        when(roleRepository.findRoleByName(anyString()))
                .thenReturn(Optional.of(role));
        // Act
        mockUserService.makeJunkieOrganizer(mockUser.getUuid());

        // Assert
        verify(userRepository, times(1))
                .updateRole(mockUser.getUuid(), role);
    }

    @Test
    void blockUser_Should_CallRepository() {
        // Arrange
        User mockUser = Helpers.createMockUser();

        // Act
        mockUserService.blockUser(mockUser.getUuid());

        // Assert
        verify(userRepository, times(1))
                .blockUser(mockUser.getUuid());
    }

    @Test
    void unblockUser_Should_CallRepository() {
        // Arrange
        User mockUser = Helpers.createMockUser();

        // Act
        mockUserService.unblockUser(mockUser.getUuid());

        // Assert
        verify(userRepository, times(1))
                .unblockUser(mockUser.getUuid());
    }

    @Test
    void addPointsToUser_Should_CallRepository() {
        // Arrange
        User mockUser = Helpers.createMockUser();
        Rank rank = Helpers.createMockRank();
        rank.setName("Master");
        rank.setPoints(1005);

        when(userRepository.findByUuid(mockUser.getUuid()))
                .thenReturn(Optional.of(mockUser));
        when(rankRepository.findRankByPoints(anyInt()))
                .thenReturn(rank);

        // Act
        mockUserService.addPointsToUser(mockUser.getUuid(), 1005);

        // Assert
        assertEquals(rank, mockUser.getRank());
        assertEquals(1060, mockUser.getPoints());
    }

    @Test
    void addPointsToUser_Should_Throw_WhenUserDoesNotExist() {
        // Arrange
        User mockUser = Helpers.createMockUser();

        when(userRepository.findByUuid(mockUser.getUuid()))
                .thenReturn(Optional.empty());

        // Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> mockUserService.addPointsToUser(mockUser.getUuid(), anyInt()));
    }

    @Test
    void rewardUsersByPlacement_ShouldCallAddPointsToUser_WhenParticipationListSizeIsGreaterThanOne() {
        Contest contest = Helpers.createMockContest();
        Participation participation1 = Helpers.createMockParticipation();
        participation1.getUser().setUuid(UUID.randomUUID());
        Participation participation2 = Helpers.createMockParticipation();
        participation2.getUser().setUuid(UUID.randomUUID());
        participation2.setParticipationId(2);
        Participation participation3 = Helpers.createMockParticipation();
        Participation participation4 = Helpers.createMockParticipation();
        participation3.getUser().setUuid(UUID.randomUUID());
        participation3.setParticipationId(3);
        participation3.setPlacement(2);
        participation4.getUser().setUuid(UUID.randomUUID());
        participation4.setParticipationId(4);
        participation4.setPlacement(2);
        Participation participation5 = Helpers.createMockParticipation();
        Participation participation6 = Helpers.createMockParticipation();
        participation5.getUser().setUuid(UUID.randomUUID());
        participation5.setParticipationId(6);
        participation5.setPlacement(3);
        participation6.getUser().setUuid(UUID.randomUUID());
        participation6.setParticipationId(6);
        participation6.setPlacement(3);

        List<Participation> participationList = new ArrayList<>();
        participationList.add(participation1);
        participationList.add(participation2);
        participationList.add(participation3);
        participationList.add(participation4);
        participationList.add(participation5);
        participationList.add(participation6);

        when(participationRepository.findAllByContestOrderByPlacement(contest))
                .thenReturn(participationList);
        when(userRepository.findByUuid(participation1.getUser().getUuid()))
                .thenReturn(Optional.ofNullable(participation1.getUser()));
        when(rankRepository.findRankByPoints(anyInt()))
                .thenReturn(participation1.getUser().getRank());
        when(userRepository.findByUuid(participation2.getUser().getUuid()))
                .thenReturn(Optional.ofNullable(participation2.getUser()));
        when(rankRepository.findRankByPoints(anyInt()))
                .thenReturn(participation2.getUser().getRank());
        when(userRepository.findByUuid(participation3.getUser().getUuid()))
                .thenReturn(Optional.ofNullable(participation3.getUser()));
        when(rankRepository.findRankByPoints(anyInt()))
                .thenReturn(participation3.getUser().getRank());
        when(userRepository.findByUuid(participation4.getUser().getUuid()))
                .thenReturn(Optional.ofNullable(participation4.getUser()));
        when(rankRepository.findRankByPoints(anyInt()))
                .thenReturn(participation4.getUser().getRank());
        when(userRepository.findByUuid(participation5.getUser().getUuid()))
                .thenReturn(Optional.ofNullable(participation5.getUser()));
        when(rankRepository.findRankByPoints(anyInt()))
                .thenReturn(participation5.getUser().getRank());
        when(userRepository.findByUuid(participation6.getUser().getUuid()))
                .thenReturn(Optional.ofNullable(participation6.getUser()));
        when(rankRepository.findRankByPoints(anyInt()))
                .thenReturn(participation6.getUser().getRank());

        mockUserService.rewardUsersByPlacement(contest);

        assertEquals(95, participation1.getUser().getPoints());
        assertEquals(95, participation2.getUser().getPoints());
        assertEquals(80, participation3.getUser().getPoints());
        assertEquals(80, participation4.getUser().getPoints());
        assertEquals(65, participation5.getUser().getPoints());
        assertEquals(65, participation6.getUser().getPoints());
    }

    @Test
    void rewardUsersByPlacement_ShouldCallAddPointsToUser_WhenParticipationListSizeIsOne() {
        Contest contest = Helpers.createMockContest();
        Participation participation1 = Helpers.createMockParticipation();
        participation1.getUser().setUuid(UUID.randomUUID());
        Participation participation2 = Helpers.createMockParticipation();
        participation2.getUser().setUuid(UUID.randomUUID());
        participation2.setParticipationId(2);
        participation2.setPlacement(2);
        Participation participation3 = Helpers.createMockParticipation();
        participation3.getUser().setUuid(UUID.randomUUID());
        participation3.setParticipationId(3);
        participation3.setPlacement(3);

        List<Participation> participationList = new ArrayList<>();
        participationList.add(participation1);
        participationList.add(participation2);
        participationList.add(participation3);

        when(participationRepository.findAllByContestOrderByPlacement(contest))
                .thenReturn(participationList);
        when(userRepository.findByUuid(participation1.getUser().getUuid()))
                .thenReturn(Optional.ofNullable(participation1.getUser()));
        when(rankRepository.findRankByPoints(anyInt()))
                .thenReturn(participation1.getUser().getRank());
        when(userRepository.findByUuid(participation2.getUser().getUuid()))
                .thenReturn(Optional.ofNullable(participation2.getUser()));
        when(rankRepository.findRankByPoints(anyInt()))
                .thenReturn(participation2.getUser().getRank());
        when(userRepository.findByUuid(participation3.getUser().getUuid()))
                .thenReturn(Optional.ofNullable(participation3.getUser()));
        when(rankRepository.findRankByPoints(anyInt()))
                .thenReturn(participation3.getUser().getRank());

        mockUserService.rewardUsersByPlacement(contest);

        assertEquals(105, participation1.getUser().getPoints());
        assertEquals(90, participation2.getUser().getPoints());
        assertEquals(75, participation3.getUser().getPoints());
    }

    @Test
    void rewardUsersByPlacement_ShouldCallAddDoubledPointsToUser_WhenParticipationListSizeIsOne() {
        Contest contest = Helpers.createMockContest();
        Participation participation1 = Helpers.createMockParticipation();
        participation1.getUser().setUuid(UUID.randomUUID());
        participation1.setAppraisal(10);
        Participation participation2 = Helpers.createMockParticipation();
        participation2.getUser().setUuid(UUID.randomUUID());
        participation2.setParticipationId(2);
        participation2.setAppraisal(4);
        participation2.setPlacement(2);
        Participation participation3 = Helpers.createMockParticipation();
        participation3.getUser().setUuid(UUID.randomUUID());
        participation3.setAppraisal(3);
        participation3.setParticipationId(3);
        participation3.setPlacement(3);

        List<Participation> participationList = new ArrayList<>();
        participationList.add(participation1);
        participationList.add(participation2);
        participationList.add(participation3);

        when(participationRepository.findAllByContestOrderByPlacement(contest))
                .thenReturn(participationList);
        when(userRepository.findByUuid(participation1.getUser().getUuid()))
                .thenReturn(Optional.ofNullable(participation1.getUser()));
        when(rankRepository.findRankByPoints(anyInt()))
                .thenReturn(participation1.getUser().getRank());
        when(userRepository.findByUuid(participation2.getUser().getUuid()))
                .thenReturn(Optional.ofNullable(participation2.getUser()));
        when(rankRepository.findRankByPoints(anyInt()))
                .thenReturn(participation2.getUser().getRank());
        when(userRepository.findByUuid(participation3.getUser().getUuid()))
                .thenReturn(Optional.ofNullable(participation3.getUser()));
        when(rankRepository.findRankByPoints(anyInt()))
                .thenReturn(participation3.getUser().getRank());

        mockUserService.rewardUsersByPlacement(contest);

        assertEquals(130, participation1.getUser().getPoints());
        assertEquals(90, participation2.getUser().getPoints());
        assertEquals(75, participation3.getUser().getPoints());
    }

    @Test
    void updatePasswordResetToken_Should_Throw_When_UserDoesNotExist() {
        User mockUser = Helpers.createMockUser();
        String token = UUID.randomUUID().toString();

        // Arrange
        when(userRepository.findByEmail(mockUser.getEmail()))
                .thenThrow(EntityNotFoundException.class);

        // Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> mockUserService.updatePasswordResetToken(token, mockUser.getEmail()));
    }

    @Test
    void updatePasswordResetToken_Should_CallRepository_When_UserExists() {
        // Arrange
        User mockUser = Helpers.createMockUser();
        String token = UUID.randomUUID().toString();
        mockUser.setPasswordResetToken(token);

        when(userRepository.findByEmail(mockUser.getEmail()))
                .thenReturn(Optional.of(mockUser));
        // Act
        mockUserService.updatePasswordResetToken(token, mockUser.getEmail());

        // Assert
        verify(userRepository, Mockito.times(1))
                .save(mockUser);
    }

    @Test
    void updatePassword_Should_Throw_When_TokenDoesNotBelongToAnyUser() {
        String token = UUID.randomUUID().toString();

        // Arrange
        when(userRepository.findByPasswordResetToken(anyString()))
                .thenThrow(EntityNotFoundException.class);

        // Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> mockUserService.updatePassword(token, anyString()));
    }

    @Test
    void updatePassword_Should_CallRepository_When_TokenBelongsToUser() {
        // Arrange
        User mockUser = Helpers.createMockUser();
        String token = UUID.randomUUID().toString();
        mockUser.setPasswordResetToken(token);

        when(userRepository.findByPasswordResetToken(token))
                .thenReturn(Optional.of(mockUser));
        // Act
        mockUserService.updatePassword(token, anyString());

        // Assert
        verify(userRepository, Mockito.times(1))
                .save(mockUser);
    }

}
