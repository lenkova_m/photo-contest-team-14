package com.company.web.photo_contest.services;

import com.company.web.photo_contest.Helpers;
import com.company.web.photo_contest.helpers.UserMapper;
import com.company.web.photo_contest.models.Role;
import com.company.web.photo_contest.modelsDto.RoleDto;
import com.company.web.photo_contest.repositories.RoleRepository;
import jakarta.persistence.EntityExistsException;
import jakarta.persistence.EntityNotFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class RoleServiceImplTests {

    @Mock
    RoleRepository roleRepository;

    @InjectMocks
    RoleServiceImpl roleService;

    @Mock
    UserMapper userMapper;

    @Test
    void getAll_Should_CallRepository() {

        // Arrange
        when(roleRepository.findAll()).thenReturn(null);

        // Act
        roleService.getAll();

        // Assert
        verify(roleRepository, Mockito.times(1))
                .findAll();
    }

    @Test
    void getRoleByName_Should_CallRepository() {
        Role role = Helpers.createMockRole();

        // Arrange
        when(roleRepository.findRoleByName(role.getName()))
                .thenReturn(Optional.of(role));

        // Act
        Role result = roleService.getRoleByName(role.getName());

        // Assert
        verify(roleRepository, Mockito.times(1))
                .findRoleByName(role.getName());
        assertEquals(role, result);

    }

    @Test
    void getRoleByName_Should_Throw_WhenRoleDoesNotExist() {
        Role role = Helpers.createMockRole();

        // Arrange
        when(roleRepository.findRoleByName(role.getName()))
                .thenReturn(Optional.empty());

        // Act, Assert
        assertThrows(EntityNotFoundException.class,
                () -> roleService.getRoleByName(role.getName()));

    }

    @Test
    void create_Should_CallRepository_When_ArgumentsAreValid() {
        Role role = Helpers.createMockRole();
        RoleDto roleDto = Helpers.createMockRoleDto();

        // Arrange
        when(userMapper.convertToRoleFromCreateDto(roleDto))
                .thenReturn(role);
        when(roleRepository.findRoleByName(role.getName()))
                .thenReturn(Optional.empty());

        // Act
        roleService.createRole(roleDto);

        // Assert
        verify(roleRepository, Mockito.times(1))
                .save(role);

    }

    @Test
    void create_Should_Throw_When_RoleExists() {
        Role role = Helpers.createMockRole();
        RoleDto roleDto = Helpers.createMockRoleDto();

        // Arrange
        when(userMapper.convertToRoleFromCreateDto(roleDto))
                .thenReturn(role);
        when(roleRepository.findRoleByName(role.getName()))
                .thenReturn(Optional.of(role));

        // Act, Assert
        assertThrows(EntityExistsException.class,
                () -> roleService.createRole(roleDto));
    }

    @Test
    void update_Should_CallRepository_When_ArgumentsAreValid() {
        Role role = Helpers.createMockRole();
        RoleDto roleDto = Helpers.createMockRoleDto();


        // Arrange
        when(userMapper.convertToRoleFromUpdateDto(role.getRoleId(), roleDto))
                .thenReturn(role);
        when(roleRepository.findRoleByName(role.getName()))
                .thenReturn(Optional.empty());

        // Act
        roleService.updateRole(role.getRoleId(), roleDto);

        // Assert
        verify(roleRepository, Mockito.times(1))
                .save(role);
    }

    @Test
    void update_Should_Throw_WhenRankExists() {
        // Arrange
        Role role = Helpers.createMockRole();
        Role existingRole = Helpers.createMockRole();
        existingRole.setRoleId(2);
        RoleDto roleDto = Helpers.createMockRoleDto();

        when(userMapper.convertToRoleFromUpdateDto(role.getRoleId(), roleDto))
                .thenReturn(role);
        when(roleRepository.findRoleByName(role.getName()))
                .thenReturn(Optional.of(existingRole));

        // Act, Assert
        assertThrows(EntityExistsException.class,
                () -> roleService.updateRole(role.getRoleId(), roleDto));
    }

    @Test
    void delete_Should_CallRepository() {
        // Arrange
        Role role = Helpers.createMockRole();

        // Act
        roleService.deleteURole(role.getRoleId());

        // Assert
        verify(roleRepository, Mockito.times(1))
                .deleteById(role.getRoleId());
    }
}
