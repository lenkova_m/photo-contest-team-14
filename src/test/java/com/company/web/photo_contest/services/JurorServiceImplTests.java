package com.company.web.photo_contest.services;

import com.company.web.photo_contest.models.Contest;
import com.company.web.photo_contest.models.Juror;
import com.company.web.photo_contest.models.User;
import com.company.web.photo_contest.repositories.ContestRepository;
import com.company.web.photo_contest.repositories.JurorRepository;
import com.company.web.photo_contest.repositories.UserRepository;
import jakarta.persistence.EntityExistsException;
import jakarta.persistence.EntityNotFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.Optional;

import static com.company.web.photo_contest.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class JurorServiceImplTests {

    @Mock
    JurorRepository mockJurorRepository;

    @Mock
    UserRepository mockUserRepository;

    @Mock
    ContestRepository mockContestRepository;

    @InjectMocks
    JurorServiceImpl jurorService;

    @Test
    void getById_Should_CallRepository() {
        Juror mockJuror = createMockJuror();

        Mockito.when(mockJurorRepository.findById(Mockito.anyInt()))
                .thenReturn(Optional.of(mockJuror));

        jurorService.getByID(Mockito.anyInt());

        Mockito.verify(mockJurorRepository, Mockito.times(1)).
                findById(Mockito.anyInt());
    }

    @Test
    void getById_Throw_WhenJurorNotExists() {
        Mockito.when(mockJurorRepository.findById(Mockito.anyInt()))
                .thenReturn(Optional.empty());

        Assertions.assertThrows(EntityNotFoundException.class,
                () -> jurorService.getByID(Mockito.anyInt()));
    }

    @Test
    void getAllByContestId_Should_CallRepository() {
        Juror mockJuror = createMockJuror();

        Mockito.when(mockJurorRepository.getAllByContestId(Mockito.anyInt()))
                .thenReturn(Collections.singletonList(mockJuror));

        jurorService.getAllByContestId(Mockito.anyInt());

        Mockito.verify(mockJurorRepository, Mockito.times(1))
                .getAllByContestId(Mockito.anyInt());
    }

    @Test
    void getByContestIdAndUserId_Should_CallRepository() {
        Contest mockContest = createMockContest();
        User mockUser = createMockUser();
        Juror mockJuror = createMockJuror();

        Mockito.when(mockContestRepository.getContestByUuid(mockContest.getUuid()))
                .thenReturn(Optional.of(mockContest));

        Mockito.when(mockUserRepository.findByUuid(mockUser.getUuid()))
                .thenReturn(Optional.of(mockUser));

        Mockito.when(mockJurorRepository.getJurorByContestIdAndUserId(Mockito.anyInt(), Mockito.anyInt()))
                .thenReturn(Optional.of(mockJuror));

        jurorService.getByContestUuidAndUserUuid(mockContest.getUuid(), mockUser.getUuid());

        Mockito.verify(mockJurorRepository, Mockito.times(1))
                .getJurorByContestIdAndUserId(Mockito.anyInt(), Mockito.anyInt());
    }

    @Test
    void getByContestIdAndUserId_Throw_WhenContestNotExists() {
        Contest mockContest = createMockContest();
        User mockUser = createMockUser();

        Mockito.when(mockContestRepository.getContestByUuid(mockContest.getUuid()))
                .thenReturn(Optional.empty());


        Assertions.assertThrows(EntityNotFoundException.class,
                () -> jurorService.getByContestUuidAndUserUuid(mockContest.getUuid(), mockUser.getUuid()));
    }

    @Test
    void getByContestIdAndUserId_Throw_WhenUserNotExists() {
        Contest mockContest = createMockContest();
        User mockUser = createMockUser();

        Mockito.when(mockContestRepository.getContestByUuid(mockContest.getUuid()))
                .thenReturn(Optional.of(mockContest));

        Mockito.when(mockUserRepository.findByUuid(mockUser.getUuid()))
                .thenReturn(Optional.empty());

        Assertions.assertThrows(EntityNotFoundException.class,
                () -> jurorService.getByContestUuidAndUserUuid(mockContest.getUuid(), mockUser.getUuid()));
    }

    @Test
    void getByContestIdAndUserId_ReturnNull_WhenJurorNotExists() {
        Contest mockContest = createMockContest();
        User mockUser = createMockUser();

        Mockito.when(mockContestRepository.getContestByUuid(mockContest.getUuid()))
                .thenReturn(Optional.of(mockContest));

        Mockito.when(mockUserRepository.findByUuid(mockUser.getUuid()))
                .thenReturn(Optional.of(mockUser));

        Mockito.when(mockJurorRepository.getJurorByContestIdAndUserId(Mockito.anyInt(), Mockito.anyInt()))
                .thenReturn(Optional.empty());

        Assertions.assertNull(jurorService.getByContestUuidAndUserUuid(mockContest.getUuid(), mockUser.getUuid()));
    }
    @Test
    void findIfUserISJury_ReturnTrue_WhenUserIsJuror() {
        User mockUser = createMockUser();
        Juror mockJuror = createMockJuror();

        Mockito.when(mockUserRepository.findByUsername(mockUser.getUsername()))
                .thenReturn(Optional.of(mockUser));

        Mockito.when(mockJurorRepository.findFirstByUserId(mockUser.getUserId()))
                .thenReturn(Optional.of(mockJuror));

        boolean result = jurorService.findIfUserIsJury(mockUser.getUsername());

        Assertions.assertTrue(result);
    }
    @Test
    void findIfUserISJury_ReturnFalse_WhenUserIsNotJuror() {
        User mockUser = createMockUser();

        Mockito.when(mockUserRepository.findByUsername(mockUser.getUsername()))
                .thenReturn(Optional.of(mockUser));

        Mockito.when(mockJurorRepository.findFirstByUserId(mockUser.getUserId()))
                .thenReturn(Optional.empty());

        boolean result = jurorService.findIfUserIsJury(mockUser.getUsername());

        Assertions.assertFalse(result);
    }

    @Test
    void create_Should_CallRepository() {
        Contest mockContest = createMockContest();
        User mockUser = createMockUser();

        Mockito.when(mockContestRepository.getContestByUuid(mockContest.getUuid()))
                .thenReturn(Optional.of(mockContest));

        Mockito.when(mockUserRepository.findByUuid(mockUser.getUuid()))
                .thenReturn(Optional.of(mockUser));

        Mockito.when(mockJurorRepository.getJurorByContestIdAndUserId(Mockito.anyInt(), Mockito.anyInt()))
                .thenReturn(Optional.empty());

        jurorService.create(mockContest.getUuid(), mockUser.getUuid());

        ArgumentCaptor<Juror> jurorCaptor = ArgumentCaptor.forClass(Juror.class);
        Mockito.verify(mockJurorRepository, Mockito.times(1)).save(jurorCaptor.capture());

        Juror capturedJuror = jurorCaptor.getValue();
        Assertions.assertEquals(mockUser.getUserId(), capturedJuror.getUserId());
        Assertions.assertEquals(mockContest.getContestId(), capturedJuror.getContestId());
    }

    @Test
    void create_Throw_WhenJurorExists() {
        Contest mockContest = createMockContest();
        User mockUser = createMockUser();

        Mockito.when(mockContestRepository.getContestByUuid(mockContest.getUuid()))
                .thenReturn(Optional.of(mockContest));

        Mockito.when(mockUserRepository.findByUuid(mockUser.getUuid()))
                .thenReturn(Optional.of(mockUser));

        Mockito.when(mockJurorRepository.getJurorByContestIdAndUserId(Mockito.anyInt(), Mockito.anyInt()))
                .thenReturn(Optional.of(createMockJuror()));

        Assertions.assertThrows(EntityExistsException.class,
                () -> jurorService.create(mockContest.getUuid(), mockUser.getUuid()));
    }

    @Test
    void delete_Should_CallRepository() {
        Juror mockJuror = createMockJuror();

        jurorService.delete(mockJuror);

        Mockito.verify(mockJurorRepository, Mockito.times(1))
                .delete(mockJuror);
    }

}
