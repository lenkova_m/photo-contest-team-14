ALTER TABLE contests
    DROP COLUMN cover_photo;

alter table contests
 add column cover_photo varchar(500);

ALTER TABLE participations
    DROP COLUMN photo;

alter table participations
    add column photo varchar(500);