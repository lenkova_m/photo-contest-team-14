INSERT INTO `categories` (`name`)
VALUES ('Category');
INSERT INTO `categories` (`name`)
VALUES ('Test');
INSERT INTO `categories` (`name`)
VALUES ('Cats');
INSERT INTO `categories` (`name`)
VALUES ('Dogs');
INSERT INTO `categories` (`name`)
VALUES ('Happy people');

INSERT INTO `rankings` (`name`, `points`)
VALUES ('Junkie', 50);
INSERT INTO `rankings` (`name`, `points`)
VALUES ('Enthusiast', 150);
INSERT INTO `rankings` (`name`, `points`)
VALUES ('Master', 1000);
INSERT INTO `rankings` (`name`, `points`)
VALUES ('Wise and Benevolent Photo Dictator', 1001);

INSERT INTO `users` (`first_name`, `last_name`, `username`, `password`, `email`, `role_id`, `rank_id`, `points`,
                     `profile_photo`, `uuid`, `is_enabled`, `is_locked`)
VALUES ('Ivan', 'Ivanov', 'ivanUser', 'pass', 'ivan@company.com', 1, 3, 152, 'profilephoto',
        'da6cd16c-dda9-4db9-8a35-77af020ba098', 0, 0);
INSERT INTO `users` (`first_name`, `last_name`, `username`, `password`, `email`, `role_id`, `rank_id`, `points`,
                     `profile_photo`, `uuid`, `is_enabled`, `is_locked`)
VALUES ('Petar', 'Petrov', 'pesho', '$2a$10$nZrzkB7SHIu27P3YQ4koJ.CzxW.A/0JLotbYOarNPQlzeWOOGVctG', 'pesho@company', 1,
        1, 0,
        'https://cdn-icons-png.flaticon.com/512/16/16363.png?w=900&t=st=1678207737~exp=1678208337~hmac=7a4a8a7c18cd9d742274b644da85a04bf7b0324672847173b5dcbc64d272edb7',
        '7ff0a7f3-0016-4ca0-9f85-a31d2ab41a18', 0, 0);
INSERT INTO `users` (`first_name`, `last_name`, `username`, `password`, `email`, `role_id`, `rank_id`, `points`,
                     `profile_photo`, `uuid`, `is_enabled`, `is_locked`)
VALUES ('Georgi', 'Georgiev', 'goshoUser', '$2a$10$UB.n5N/J5SexQFASqURF1.4x5Y3ADKJiWQfYLgcIkENkujKsO5cX6',
        'georgi@company.com', 1, 1, 0,
        'https://cdn-icons-png.flaticon.com/512/16/16363.png?w=900&t=st=1678207737~exp=1678208337~hmac=7a4a8a7c18cd9d742274b644da85a04bf7b0324672847173b5dcbc64d272edb7',
        '6fb15ae6-5b2b-4a48-b9ef-762d234ce236', 0, 0);
INSERT INTO `users` (`first_name`, `last_name`, `username`, `password`, `email`, `role_id`, `rank_id`, `points`,
                     `profile_photo`, `uuid`, `is_enabled`, `is_locked`)
VALUES ('Monika', 'Dimitrova', 'monikaUser', '$2a$10$LTC6RrueeZhy6N/UIeD4I..xGqoipole/WIjfaZDDV4LDz.502ruy',
        'monik@company.com', 1, 1, 0,
        'https://cdn-icons-png.flaticon.com/512/16/16363.png?w=900&t=st=1678207737~exp=1678208337~hmac=7a4a8a7c18cd9d742274b644da85a04bf7b0324672847173b5dcbc64d272edb7',
        '9502a0d2-9de7-4a63-afad-bfed29a28e5a', 0, 0);
INSERT INTO `users` (`first_name`, `last_name`, `username`, `password`, `email`, `role_id`, `rank_id`, `points`,
                     `profile_photo`, `uuid`, `is_enabled`, `is_locked`)
VALUES ('Vesela', 'Todorova', 'veselaUser', '$2a$10$ppIYNfl0TF8nuphXSp2VJ.vu6SgHVqqAS1HxFezFPcW9eDvbhSqBi',
        'vesela@company.com', 1, 1, 0,
        'https://cdn-icons-png.flaticon.com/512/16/16363.png?w=900&t=st=1678207737~exp=1678208337~hmac=7a4a8a7c18cd9d742274b644da85a04bf7b0324672847173b5dcbc64d272edb7',
        '393a1d85-e39b-4175-843a-3dee8d6e0a6c', 0, 0);
INSERT INTO `users` (`first_name`, `last_name`, `username`, `password`, `email`, `role_id`, `rank_id`, `points`,
                     `profile_photo`, `uuid`, `is_enabled`, `is_locked`)
VALUES ('Paolina', 'Stoyanova', 'polyUser', '$2a$10$jTby6OE7JiPF..Fqx55wIOfaVef/DtnqFdSfapL7CzC4zONmuKDwC',
        'poly@company.com', 1, 1, 0,
        'https://cdn-icons-png.flaticon.com/512/16/16363.png?w=900&t=st=1678207737~exp=1678208337~hmac=7a4a8a7c18cd9d742274b644da85a04bf7b0324672847173b5dcbc64d272edb7',
        'd356f69d-be46-4c8a-b988-4e24edc09d62', 0, 0);

INSERT INTO `contests` (`title`, `category_id`, `date_created`, `phase_one`, `phase_two`, `is_finished`, `user_id`,
                        `is_invitational`, `is_graded`, `cover_photo`, `uuid`)
VALUES ('New', 1, '2023-04-08 13:14:22', '2023-04-08 13:14:26', '2024-04-08 13:14:23', 1, (select user_id from users where username = 'ivanUser'), 0, 0, '\0',
        'dd021a6c-b223-42a7-ad15-f18165bce303');
INSERT INTO `contests` (`title`, `category_id`, `date_created`, `phase_one`, `phase_two`, `is_finished`, `user_id`,
                        `is_invitational`, `is_graded`, `cover_photo`, `uuid`)
VALUES ('Contest', 2, '2023-04-08 13:14:22', '2023-04-08 13:14:26', '2024-04-08 13:14:23', 0, (select user_id from users where username = 'pesho'), 0, 0, '0x00',
        'dbf02cab-5c99-401a-93c8-7d0ddc49e659');
INSERT INTO `contests` (`title`, `category_id`, `date_created`, `phase_one`, `phase_two`, `is_finished`, `user_id`,
                        `is_invitational`, `is_graded`, `cover_photo`, `uuid`)
VALUES (' Photo of the year', 3, '2023-04-11 16:02:39', '2023-04-11 13:14:26', '2024-04-13 13:14:23', 1, (select user_id from users where username = 'ivanUser'), 0, 0,
        '0x00', 'd3c5778d-c28d-44c1-8bd8-c74a3fc4e675');
INSERT INTO `contests` (`title`, `category_id`, `date_created`, `phase_one`, `phase_two`, `is_finished`, `user_id`,
                        `is_invitational`, `is_graded`, `cover_photo`, `uuid`)
VALUES ('Sleepy dogs', 4, '2023-04-11 16:09:25', '2023-04-11 13:14:26', '2024-04-13 13:14:23', 1, (select user_id from users where username = 'goshoUser'), 0, 0, '0x00',
        '1619df9f-9663-4cfc-8241-7e95ed817ccf');
INSERT INTO `contests` (`title`, `category_id`, `date_created`, `phase_one`, `phase_two`, `is_finished`, `user_id`,
                        `is_invitational`, `is_graded`, `cover_photo`, `uuid`)
VALUES ('Funny cats update', 3, '2023-04-11 17:02:44', '2023-04-11 16:49:26', '2024-04-12 13:14:23', 0, (select user_id from users where username = 'pesho'), 1, 0, '0x00',
        '91932b9c-5844-45bf-b857-f2235bf89000');
INSERT INTO `contests` (`title`, `category_id`, `date_created`, `phase_one`, `phase_two`, `is_finished`, `user_id`,
                        `is_invitational`, `is_graded`, `cover_photo`, `uuid`)
VALUES ('Flowers', 1, '2023-04-11 17:14:19', '2023-04-13 16:49:26', '2024-04-15 01:14:23', 0, (select user_id from users where username = 'veselaUser'), 1, 0, '0x00',
        '7dcb3365-fb37-404b-939c-883370152fae');

