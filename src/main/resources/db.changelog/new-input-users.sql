
INSERT INTO `users` (`first_name`, `last_name`, `username`, `password`, `email`, `role_id`, `rank_id`, `points`,
    `profile_photo`, `uuid`, `is_enabled`, `is_locked`)
VALUES ('Daniela', 'Mihaylova', 'misheto', '$2a$10$nZrzkB7SHIu27P3YQ4koJ.CzxW.A/0JLotbYOarNPQlzeWOOGVctG', 'mishe@company.com', 2, 1, 31,
    'https://img.freepik.com/premium-vector/holding-camera-with-two-hands-illustration-cartoon-vector_484148-252.jpg?w=740',
    '7d089ce9-fb35-4e82-9207-daaa593fedad', 1, 0);
INSERT INTO `users` (`first_name`, `last_name`, `username`, `password`, `email`, `role_id`, `rank_id`, `points`,
    `profile_photo`, `uuid`, `is_enabled`, `is_locked`)
VALUES ('Zdravko', 'Blagoev', 'zdravec', '$2a$10$nZrzkB7SHIu27P3YQ4koJ.CzxW.A/0JLotbYOarNPQlzeWOOGVctG', 'zdravko@company.com', 2, 3, 230,
    'https://img.freepik.com/premium-vector/holding-camera-with-two-hands-illustration-cartoon-vector_484148-252.jpg?w=740',
    'fc62fd4e-3fee-48e5-8980-ee9bbed1eab0', 1, 0);
INSERT INTO `users` (`first_name`, `last_name`, `username`, `password`, `email`, `role_id`, `rank_id`, `points`,
    `profile_photo`, `uuid`, `is_enabled`, `is_locked`)
VALUES ('Velislava', 'Borisova', 'veliUser', '$2a$10$UB.n5N/J5SexQFASqURF1.4x5Y3ADKJiWQfYLgcIkENkujKsO5cX6','veli@company.com', 2, 1, 0,
    'https://img.freepik.com/premium-vector/holding-camera-with-two-hands-illustration-cartoon-vector_484148-252.jpg?w=740',
    '0221ea7b-4823-4f9f-af59-25a7dca8dfe6', 1, 0);
INSERT INTO `users` (`first_name`, `last_name`, `username`, `password`, `email`, `role_id`, `rank_id`, `points`,
    `profile_photo`, `uuid`, `is_enabled`, `is_locked`)
VALUES ('Stanislav', 'Velkov', 'staniUser', '$2a$10$LTC6RrueeZhy6N/UIeD4I..xGqoipole/WIjfaZDDV4LDz.502ruy','stani@company.com', 2, 2, 70,
    'https://img.freepik.com/premium-vector/holding-camera-with-two-hands-illustration-cartoon-vector_484148-252.jpg?w=740',
    '6c461330-a945-4a8b-8624-f85708c017f4', 1, 0);
INSERT INTO `users` (`first_name`, `last_name`, `username`, `password`, `email`, `role_id`, `rank_id`, `points`,
    `profile_photo`, `uuid`, `is_enabled`, `is_locked`)
VALUES ('Bilyana', 'Hristova', 'bibi', '$2a$10$ppIYNfl0TF8nuphXSp2VJ.vu6SgHVqqAS1HxFezFPcW9eDvbhSqBi','bibi@company.com', 1, 3, 666,
    'https://img.freepik.com/premium-vector/holding-camera-with-two-hands-illustration-cartoon-vector_484148-252.jpg?w=740',
    '43e8a145-3d82-4e33-9ee4-560ad893fbfe', 1, 0);
INSERT INTO `users` (`first_name`, `last_name`, `username`, `password`, `email`, `role_id`, `rank_id`, `points`,
    `profile_photo`, `uuid`, `is_enabled`, `is_locked`)
VALUES ('Mihail', 'Nikolaev', 'misho', '$2a$10$jTby6OE7JiPF..Fqx55wIOfaVef/DtnqFdSfapL7CzC4zONmuKDwC','misho@company.com', 2, 2, 55,
    'https://img.freepik.com/premium-vector/holding-camera-with-two-hands-illustration-cartoon-vector_484148-252.jpg?w=740',
    '7a0397f9-3408-4ab0-b49a-45578be9c488', 1, 0);
INSERT INTO `users` (`first_name`, `last_name`, `username`, `password`, `email`, `role_id`, `rank_id`, `points`,
    `profile_photo`, `uuid`, `is_enabled`, `is_locked`)
VALUES ('Desislava', 'Georgieva', 'desi', '$2a$10$UB.n5N/J5SexQFASqURF1.4x5Y3ADKJiWQfYLgcIkENkujKsO5cX6','desi@company.com', 2, 1, 0,
    'https://img.freepik.com/premium-vector/holding-camera-with-two-hands-illustration-cartoon-vector_484148-252.jpg?w=740',
    'b3e98b0d-bf9b-4ade-9ee3-c554c7f3c944', 1, 0);
INSERT INTO `users` (`first_name`, `last_name`, `username`, `password`, `email`, `role_id`, `rank_id`, `points`,
    `profile_photo`, `uuid`, `is_enabled`, `is_locked`)
VALUES ('Simeon', 'Iliev', 'simo', '$2a$10$UB.n5N/J5SexQFASqURF1.4x5Y3ADKJiWQfYLgcIkENkujKsO5cX6','simo@company.com', 2, 1, 0,
    'https://img.freepik.com/premium-vector/holding-camera-with-two-hands-illustration-cartoon-vector_484148-252.jpg?w=740',
    'b978fdc1-2ace-42c1-a3bb-d23b31509a86', 1, 0);
INSERT INTO `users` (`first_name`, `last_name`, `username`, `password`, `email`, `role_id`, `rank_id`, `points`,
    `profile_photo`, `uuid`, `is_enabled`, `is_locked`)
VALUES ('Galina', 'Stanimirova', 'gali', '$2a$10$UB.n5N/J5SexQFASqURF1.4x5Y3ADKJiWQfYLgcIkENkujKsO5cX6','gali@company.com', 2, 1, 0,
    'https://img.freepik.com/premium-vector/holding-camera-with-two-hands-illustration-cartoon-vector_484148-252.jpg?w=740',
    'f4ef4478-4906-43ba-934d-d6b42a4af2b3', 1, 0);
INSERT INTO `users` (`first_name`, `last_name`, `username`, `password`, `email`, `role_id`, `rank_id`, `points`,
    `profile_photo`, `uuid`, `is_enabled`, `is_locked`)
VALUES ('Dimitar', 'Dimitrov', 'mitko', '$2a$10$UB.n5N/J5SexQFASqURF1.4x5Y3ADKJiWQfYLgcIkENkujKsO5cX6','mitko@company.com', 1, 4, 1993,
    'https://img.freepik.com/premium-vector/holding-camera-with-two-hands-illustration-cartoon-vector_484148-252.jpg?w=740',
    'f534a69b-cb02-4f0f-a836-e6de953e12f5', 1, 0);
INSERT INTO `users` (`first_name`, `last_name`, `username`, `password`, `email`, `role_id`, `rank_id`, `points`,
    `profile_photo`, `uuid`, `is_enabled`, `is_locked`)
VALUES ('Metodi', 'Avramov', 'meto', '$2a$10$UB.n5N/J5SexQFASqURF1.4x5Y3ADKJiWQfYLgcIkENkujKsO5cX6','meto@company.com', 2, 1, 0,
    'https://img.freepik.com/premium-vector/holding-camera-with-two-hands-illustration-cartoon-vector_484148-252.jpg?w=740',
    '6fb24ae6-5b2b-4a48-b9ef-762d694ce236', 1, 0);