create table `photo-contest`.categories
(
    category_id int auto_increment
        primary key,
    name        varchar(50) not null
);

create table `photo-contest`.rankings
(
    rank_id int auto_increment
        primary key,
    name    varchar(50) not null,
    points  int         not null
);

create table `photo-contest`.roles
(
    role_id int auto_increment
        primary key,
    name    varchar(20) null
);

create table `photo-contest`.users
(
    user_id       int auto_increment
        primary key,
    first_name    varchar(20)  not null,
    last_name     varchar(20)  not null,
    username      varchar(20)  not null,
    password      varchar(255) not null,
    email         varchar(255) not null,
    role_id       int          not null,
    rank_id       int          not null,
    points        int          not null,
    profile_photo varchar(500) not null,
    uuid          char(36)     null,
    is_enabled    tinyint(1)   not null,
    is_locked     tinyint(1)   not null,
    constraint users_pk2
        unique (username),
    constraint users_pk3
        unique (email),
    constraint users_rankings_fk
        foreign key (rank_id) references `photo-contest`.rankings (rank_id),
    constraint users_roles_fk
        foreign key (role_id) references `photo-contest`.roles (role_id)
);

create table `photo-contest`.contests
(
    contest_id      int auto_increment
        primary key,
    title           varchar(255) not null,
    category_id     int          not null,
    date_created    datetime     not null,
    phase_one       datetime     not null,
    phase_two       datetime     not null,
    is_finished     tinyint(1)   not null,
    user_id         int          not null,
    is_invitational tinyint(1)   not null,
    is_graded       tinyint(1)   not null,
    cover_photo     blob         not null,
    uuid            char(36)     null,
    constraint contests_categories_fk
        foreign key (category_id) references `photo-contest`.categories (category_id),
    constraint contests_users_fk
        foreign key (user_id) references `photo-contest`.users (user_id)
);

create table `photo-contest`.jury
(
    juror_id   int auto_increment
        primary key,
    user_id    int not null,
    contest_id int not null,
    constraint jury_contests_fk
        foreign key (contest_id) references `photo-contest`.contests (contest_id),
    constraint jury_users_fk
        foreign key (user_id) references `photo-contest`.users (user_id)
);

create table `photo-contest`.participations
(
    participation_id int auto_increment
        primary key,
    title            varchar(255) not null,
    story            varchar(500) not null,
    date_created     datetime     not null,
    contest_id       int          not null,
    user_id          int          not null,
    appraisal        float        not null,
    placement        int          not null,
    photo            blob         not null,
    constraint participations_contests_fk
        foreign key (contest_id) references `photo-contest`.contests (contest_id),
    constraint participations_users_fk
        foreign key (user_id) references `photo-contest`.users (user_id)
);

create table `photo-contest`.evaluations
(
    evaluation_id    int auto_increment
        primary key,
    rating           int          not null,
    comment          varchar(500) not null,
    date_created     datetime     not null,
    participation_id int          not null,
    juror_id         int          not null,
    constraint evaluations_jury_fk
        foreign key (juror_id) references `photo-contest`.jury (juror_id),
    constraint evaluations_participations_fk
        foreign key (participation_id) references `photo-contest`.participations (participation_id)
);

