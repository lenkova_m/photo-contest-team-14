INSERT INTO `ranks` (`rank_id`,`name`, `points`)
VALUES (1, 'Junkie', 0);
INSERT INTO `ranks` (`rank_id`,`name`, `points`)
VALUES (2, 'Enthusiast', 51);
INSERT INTO `ranks` (`rank_id`,`name`, `points`)
VALUES (3, 'Master', 151);
INSERT INTO `ranks` (`rank_id`,`name`, `points`)
VALUES (4, 'Wise and Benevolent Photo Dictator', 1001);

INSERT INTO `users` (`first_name`, `last_name`, `username`, `password`, `email`, `role_id`, `rank_id`, `points`,
                     `profile_photo`, `uuid`, `is_enabled`, `is_locked`)
VALUES ('Ivan', 'Ivanov', 'ivanUser', '$2a$10$nZrzkB7SHIu27P3YQ4koJ.CzxW.A/0JLotbYOarNPQlzeWOOGVctG', 'ivan@company.com', 1, 3, 152,
        'https://img.freepik.com/premium-vector/holding-camera-with-two-hands-illustration-cartoon-vector_484148-252.jpg?w=740',
        'da6cd16c-dda9-4db9-8a35-77af020ba098', 1, 0);
INSERT INTO `users` (`first_name`, `last_name`, `username`, `password`, `email`, `role_id`, `rank_id`, `points`,
                     `profile_photo`, `uuid`, `is_enabled`, `is_locked`)
VALUES ('Petar', 'Petrov', 'pesho', '$2a$10$nZrzkB7SHIu27P3YQ4koJ.CzxW.A/0JLotbYOarNPQlzeWOOGVctG', 'pesho@company.com', 1, 3, 160,
        'https://img.freepik.com/premium-vector/holding-camera-with-two-hands-illustration-cartoon-vector_484148-252.jpg?w=740',
        '7ff0a7f3-0016-4ca0-9f85-a31d2ab41a18', 1, 0);
INSERT INTO `users` (`first_name`, `last_name`, `username`, `password`, `email`, `role_id`, `rank_id`, `points`,
                     `profile_photo`, `uuid`, `is_enabled`, `is_locked`)
VALUES ('Georgi', 'Georgiev', 'goshoUser', '$2a$10$UB.n5N/J5SexQFASqURF1.4x5Y3ADKJiWQfYLgcIkENkujKsO5cX6','georgi@company.com', 2, 1, 0,
        'https://img.freepik.com/premium-vector/holding-camera-with-two-hands-illustration-cartoon-vector_484148-252.jpg?w=740',
        '6fb15ae6-5b2b-4a48-b9ef-762d234ce236', 1, 0);
INSERT INTO `users` (`first_name`, `last_name`, `username`, `password`, `email`, `role_id`, `rank_id`, `points`,
                     `profile_photo`, `uuid`, `is_enabled`, `is_locked`)
VALUES ('Monika', 'Dimitrova', 'monikaUser', '$2a$10$LTC6RrueeZhy6N/UIeD4I..xGqoipole/WIjfaZDDV4LDz.502ruy','monik@company.com', 2, 4, 1200,
        'https://img.freepik.com/premium-vector/holding-camera-with-two-hands-illustration-cartoon-vector_484148-252.jpg?w=740',
        '9502a0d2-9de7-4a63-afad-bfed29a28e5a', 1, 0);
INSERT INTO `users` (`first_name`, `last_name`, `username`, `password`, `email`, `role_id`, `rank_id`, `points`,
                     `profile_photo`, `uuid`, `is_enabled`, `is_locked`)
VALUES ('Vesela', 'Todorova', 'veselaUser', '$2a$10$ppIYNfl0TF8nuphXSp2VJ.vu6SgHVqqAS1HxFezFPcW9eDvbhSqBi','vesela@company.com', 2, 3, 500,
        'https://img.freepik.com/premium-vector/holding-camera-with-two-hands-illustration-cartoon-vector_484148-252.jpg?w=740',
        '393a1d85-e39b-4175-843a-3dee8d6e0a6c', 1, 0);
INSERT INTO `users` (`first_name`, `last_name`, `username`, `password`, `email`, `role_id`, `rank_id`, `points`,
                     `profile_photo`, `uuid`, `is_enabled`, `is_locked`)
VALUES ('Paolina', 'Stoyanova', 'polyUser', '$2a$10$jTby6OE7JiPF..Fqx55wIOfaVef/DtnqFdSfapL7CzC4zONmuKDwC','poly@company.com', 2, 3, 500,
        'https://img.freepik.com/premium-vector/holding-camera-with-two-hands-illustration-cartoon-vector_484148-252.jpg?w=740',
        'd356f69d-be46-4c8a-b988-4e24edc09d62', 1, 0);
INSERT INTO `users` (`first_name`, `last_name`, `username`, `password`, `email`, `role_id`, `rank_id`, `points`,
                     `profile_photo`, `uuid`, `is_enabled`, `is_locked`)
VALUES ('Andrei', 'Georgiev', 'andrei', '$2a$10$UB.n5N/J5SexQFASqURF1.4x5Y3ADKJiWQfYLgcIkENkujKsO5cX6','andrei@company.com', 2, 1, 0,
        'https://img.freepik.com/premium-vector/holding-camera-with-two-hands-illustration-cartoon-vector_484148-252.jpg?w=740',
        '6fb15ae6-5b2b-4a48-b9ef-762d234ce951', 1, 0);
INSERT INTO `users` (`first_name`, `last_name`, `username`, `password`, `email`, `role_id`, `rank_id`, `points`,
                     `profile_photo`, `uuid`, `is_enabled`, `is_locked`)
VALUES ('Kaloyan', 'Kirov', 'koko', '$2a$10$UB.n5N/J5SexQFASqURF1.4x5Y3ADKJiWQfYLgcIkENkujKsO5cX6','koko@company.com', 2, 1, 0,
        'https://img.freepik.com/premium-vector/holding-camera-with-two-hands-illustration-cartoon-vector_484148-252.jpg?w=740',
        '6fb15ae6-5b2b-4a79-b9ef-762d234ce236', 1, 0);
INSERT INTO `users` (`first_name`, `last_name`, `username`, `password`, `email`, `role_id`, `rank_id`, `points`,
                     `profile_photo`, `uuid`, `is_enabled`, `is_locked`)
VALUES ('Svetlozar', 'Ivanov', 'svetlio', '$2a$10$UB.n5N/J5SexQFASqURF1.4x5Y3ADKJiWQfYLgcIkENkujKsO5cX6','svetlio@company.com', 2, 1, 0,
        'https://img.freepik.com/premium-vector/holding-camera-with-two-hands-illustration-cartoon-vector_484148-252.jpg?w=740',
        '6fb15ae6-5b2b-4a48-a5ef-762d234ce236', 1, 0);
INSERT INTO `users` (`first_name`, `last_name`, `username`, `password`, `email`, `role_id`, `rank_id`, `points`,
                     `profile_photo`, `uuid`, `is_enabled`, `is_locked`)
VALUES ('Vilian', 'Terziev', 'vilko', '$2a$10$UB.n5N/J5SexQFASqURF1.4x5Y3ADKJiWQfYLgcIkENkujKsO5cX6','vilko@company.com', 2, 1, 0,
        'https://img.freepik.com/premium-vector/holding-camera-with-two-hands-illustration-cartoon-vector_484148-252.jpg?w=740',
        '6fb15ae6-5b2b-4a48-b9ef-561d234ce236', 1, 0);
INSERT INTO `users` (`first_name`, `last_name`, `username`, `password`, `email`, `role_id`, `rank_id`, `points`,
                     `profile_photo`, `uuid`, `is_enabled`, `is_locked`)
VALUES ('Mladen', 'Georgiev', 'mladen', '$2a$10$UB.n5N/J5SexQFASqURF1.4x5Y3ADKJiWQfYLgcIkENkujKsO5cX6','mladen@company.com', 2, 1, 0,
        'https://img.freepik.com/premium-vector/holding-camera-with-two-hands-illustration-cartoon-vector_484148-252.jpg?w=740',
        '6fb24ae6-5b2b-4a48-b9ef-762d694ce236', 1, 0);

