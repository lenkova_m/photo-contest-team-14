// // Get the radio buttons and the containers for the input fields
// const radioButtons = document.querySelectorAll('input[type="radio"][name="photo-option"]');
// const fileContainer = document.getElementById('photo-file-container');
// const selectedCoverPhotoContainer = document.getElementById('selected-cover-photo-container');
// const coverPhotoUrlContainer = document.getElementById('cover-photo-url-container');
//
// // Add event listener to the radio buttons
// radioButtons.forEach(function(radio) {
//     radio.addEventListener('change', function() {
//         if (radio.value === 'file') {
//             fileContainer.style.display = 'block';
//             selectedCoverPhotoContainer.style.display = 'none';
//             coverPhotoUrlContainer.style.display = 'none';
//         } else if (radio.value === 'select') {
//             fileContainer.style.display = 'none';
//             selectedCoverPhotoContainer.style.display = 'block';
//             coverPhotoUrlContainer.style.display = 'none';
//         } else if (radio.value === 'url') {
//             fileContainer.style.display = 'none';
//             selectedCoverPhotoContainer.style.display = 'none';
//             coverPhotoUrlContainer.style.display = 'block';
//         }
//     });
// });
//
// // Trigger the change event on page load to display the correct input field
// radioButtons.forEach(function(radio) {
//     if (radio.checked) {
//         radio.dispatchEvent(new Event('change'));
//     }
// });

//
//     // add the JavaScript code here
//     $(document).ready(function() {
//     $('#photo-option-1').click(function() {
//         $('#photo-file-container').show();
//         $('#selected-cover-photo-container').hide();
//         $('#cover-photo-url-container').hide();
//     });
//
//     $('#photo-option-2').click(function() {
//     $('#photo-file-container').hide();
//     $('#selected-cover-photo-container').show();
//     $('#cover-photo-url-container').hide();
// });
//
//     $('#photo-option-3').click(function() {
//     $('#photo-file-container').hide();
//     $('#selected-cover-photo-container').hide();
//     $('#cover-photo-url-container').show();
// });
// });

    // get the radio buttons and input containers
    const radioButtons = document.querySelectorAll('input[type="radio"][name="radio"]');
    const fileInputContainer = document.querySelector('#photo-file-container');
    const selectInputContainer = document.querySelector('#selected-cover-photo-container');
    const urlInputContainer = document.querySelector('#cover-photo-url-container');

    // loop through the radio buttons and add event listener to each one
    radioButtons.forEach(function(radioButton) {
    radioButton.addEventListener('change', function() {
        // hide all input containers
        fileInputContainer.style.display = 'none';
        selectInputContainer.style.display = 'none';
        urlInputContainer.style.display = 'none';

        // show the corresponding input container based on the selected radio button
        if (radioButton.value === 'photo') {
            fileInputContainer.style.display = 'block';
        } else if (radioButton.value === 'select') {
            selectInputContainer.style.display = 'block';
        } else if (radioButton.value === 'url') {
            urlInputContainer.style.display = 'block';
        }
    });
});

