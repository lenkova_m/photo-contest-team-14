// Get the input elements for the title and story
const titlePhotoInput = document.getElementById('name');
const storyPhotoInput = document.getElementById('story');

// Get the preview elements for the title and story
const previewPhotoTitle = document.getElementById('preview-name');
const previewPhotoStory = document.getElementById('preview-story');

// Update the preview section when the user types in the input fields
titlePhotoInput.addEventListener('input', () => {
    previewPhotoTitle.innerHTML = titleInput.value;
});

storyPhotoInput.addEventListener('input', () => {
    previewPhotoStory.innerHTML = storyInput.value;
});
