package com.company.web.photo_contest.helpers;

import com.company.web.photo_contest.models.Contest;
import com.company.web.photo_contest.models.Evaluation;
import com.company.web.photo_contest.models.Participation;
import com.company.web.photo_contest.models.User;
import com.company.web.photo_contest.modelsDto.ParticipationDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

@AllArgsConstructor
@Component
public class ParticipationMapper {

    public Participation fromDto(Contest contest, ParticipationDto participationDto, User user) {
        Participation participation = new Participation();
        participation.setUser(user);
        participation.setContest(contest);
        participation.setPhoto(participationDto.getPhoto());
        participation.setDateCreated(LocalDateTime.now());
        participation.setStory(participationDto.getStory());
        participation.setTitle(participationDto.getTitle());
        int currentUserPoints = participation.getUser().getPoints();
        participation.getUser().setPoints(currentUserPoints + 1);
        return participation;
    }

    public ParticipationDto viewParticipation(Participation participation, List<Evaluation> evaluations) {
        ParticipationDto participationDto = new ParticipationDto();

        participationDto.setTitle(participation.getTitle());
        participationDto.setStory(participation.getStory());
        participationDto.setContestUuid(participation.getContest().getUuid());
        participationDto.setUser(participation.getUser());
        participationDto.setPhoto(participation.getPhoto());
        participationDto.setParticipationUuid(participation.getUuid());
        participationDto.setAppraisal(participation.getAppraisal());
        participationDto.setCommentsCount(evaluations.size());

        return participationDto;
    }
}
