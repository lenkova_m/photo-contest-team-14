package com.company.web.photo_contest.helpers;

import com.company.web.photo_contest.models.Contest;
import com.company.web.photo_contest.models.Participation;
import com.company.web.photo_contest.services.contracts.ContestService;
import com.company.web.photo_contest.services.contracts.EvaluationService;
import com.company.web.photo_contest.services.contracts.ParticipationService;
import com.company.web.photo_contest.services.contracts.UserService;
import lombok.AllArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

@Component
@AllArgsConstructor
class ContestScoringHelper {

    private final ContestService contestService;
    private final ParticipationService participationService;
    private final UserService userService;
    private final EvaluationService evaluationService;

    @Scheduled(fixedRate = 5000)
    private void scoreFinishedContests() {
        LocalDateTime now = LocalDateTime.now();
        List<Contest> contests = contestService.isCompleted(now);
        for (Contest contest : contests) {
            if (!contest.isGraded()) {
                List<Participation> participationsForContest = participationService.
                        getAllParticipationsForContest(contest.getUuid());
                for (Participation participation : participationsForContest) {
                    evaluationService.createDefaultEvaluation(participation.getUuid());
                }
                userService.rewardUsersByPlacement(contest);
                contestService.changeIsGraded(contest.getUuid());
            }
        }
    }
}
