package com.company.web.photo_contest.helpers;

import com.company.web.photo_contest.models.User;
import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import jakarta.servlet.http.HttpServletRequest;
import lombok.AllArgsConstructor;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import java.io.UnsupportedEncodingException;

@Component
@AllArgsConstructor
public class JavaMailSenderHelper {

    public static final String TEAM_EMAIL = "photocontestteam14@gmail.com";
    public static final String FRAME_FRENZY_SUPPORT = "FrameFrenzy Support";
    public static final String VERIFY_YOUR_EMAIL = "Verify your Email";
    public static final String RESET_YOUR_PASSWORD = "Reset your Password";
    private final JavaMailSender mailSender;

    public void sendVerificationEmail(User user, String link)
            throws MessagingException, UnsupportedEncodingException {
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);

        helper.setFrom(TEAM_EMAIL, FRAME_FRENZY_SUPPORT);
        helper.setTo(user.getEmail());
        helper.setSubject(VERIFY_YOUR_EMAIL);

        String content = "Dear \"" + user.getFirstName() + "\",<br>"
                + "Thank you for joining our community. <br>"
                + "Please click the link below to verify your account:<br>"
                + "<h3><a href=\"" + link + "\">Verify account</a></h3>"
                + "Best regards,<br>"
                + "FrameFrenzy Support.";
        helper.setText(content, true);

        mailSender.send(message);
    }


    public void sendResetPasswordEmail(String recipientEmail, String link)
            throws UnsupportedEncodingException, MessagingException {
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);

        helper.setFrom(TEAM_EMAIL, FRAME_FRENZY_SUPPORT);
        helper.setTo(recipientEmail);
        helper.setSubject(RESET_YOUR_PASSWORD);

        String content = "Hello,<br>"
                + "You have requested to reset your password.<br>"
                + "Please click the link below to change your password:<br>"
                + "<h3><a href=\"" + link + "\">Reset my password</a></h3><br>"
                + "If you did not make this request then please ignore this email.<br>"
                + "Best regards,<br>"
                + "FrameFrenzy Support";
        helper.setText(content, true);

        mailSender.send(message);
    }

    public String getSiteURL(HttpServletRequest request) {
        String siteURL = request.getRequestURL().toString();
        return siteURL.replace(request.getServletPath(), "");
    }
}
