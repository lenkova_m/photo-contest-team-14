package com.company.web.photo_contest.helpers;

import com.company.web.photo_contest.modelsDto.ParticipationDto;
import lombok.AllArgsConstructor;
import org.apache.tomcat.util.http.fileupload.FileUploadException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.core.exception.SdkClientException;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.*;
import software.amazon.awssdk.utils.IoUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
@AllArgsConstructor
public class AmazonS3UploadUtil {
    private static final List<String> ALLOWED_EXTENSIONS = Arrays.asList(".jpg", ".jpeg", ".png");
    private static final String BUCKET = "photocontestteam14";
    public static final String ACCESS_KEY_ID = "AKIATZHARCRCEWHURSEK";
    public static final String SECRET_ACCESS_KEY = "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";
    public static final String BUCKET_URL_PATH = "https://photocontestteam14.s3.eu-north-1.amazonaws.com/";
    public static final String USERS_PATH = "users/";
    public static final String CONTESTS_PATH = "contests/";
    public static final String PHOTO_FORMAT_ERROR_MESSAGE = "You can only upload photos.";
    public static final String UNSUPPORTED_FILE_TYPE_ERROR_MESSAGE = "Unsupported file type: ";
    private final Region region = Region.EU_NORTH_1;

    public String uploadPhotoFromUser(String username, MultipartFile multipartFile) throws IOException {
        String existingPhotoUrl = getProfilePhotoUrl(username);
        if (existingPhotoUrl != null && multipartFile.isEmpty()) {
            return existingPhotoUrl;
        } else if (multipartFile.isEmpty()) {
            return null;
        }
        String fileExtension = checkIfFileIsPhoto(multipartFile);
        String fileName = multipartFile.getOriginalFilename();

        return uploadPhotoForUser(username, fileName, fileExtension, multipartFile.getInputStream());
    }

    private String getProfilePhotoUrl(String username) {
        AwsBasicCredentials awsCredentials = AwsBasicCredentials.create(ACCESS_KEY_ID, SECRET_ACCESS_KEY);
        try (S3Client s3Client = S3Client.builder()
                .region(region)
                .credentialsProvider(StaticCredentialsProvider
                        .create(awsCredentials))
                .build()) {

            ListObjectsV2Request listObjectsRequest = ListObjectsV2Request.builder()
                    .bucket(BUCKET)
                    .prefix(USERS_PATH + username)
                    .build();
            ListObjectsV2Response listObjectsResponse = s3Client.listObjectsV2(listObjectsRequest);

            for (S3Object content : listObjectsResponse.contents()) {
                String key = content.key();
                if (key.contains(".jpg") || key.contains(".png") || key.contains(".jpeg")) {
                    return BUCKET_URL_PATH + key;
                }
            }
        }
        return null;
    }

    public String uploadPhotoForUser(String username, String fileName, String fileExtension, InputStream inputStream)
            throws IOException {
        AwsBasicCredentials awsCredentials = AwsBasicCredentials.create(ACCESS_KEY_ID, SECRET_ACCESS_KEY);
        try (S3Client s3Client = S3Client
                .builder()
                .region(region)
                .credentialsProvider(StaticCredentialsProvider
                        .create(awsCredentials))
                .build()) {

            byte[] contentBytes = IoUtils.toByteArray(inputStream);
            long contentLength = contentBytes.length;
            String contentType = getContentType(fileExtension);

            String userKey = USERS_PATH + username + "/" + fileName + fileExtension;

            s3Client.putObject(PutObjectRequest.builder()
                            .bucket(BUCKET)
                            .key(userKey)
                            .acl(ObjectCannedACL.PUBLIC_READ)
                            .contentType(contentType)
                            .contentLength(contentLength)
                            .build(),
                    RequestBody.fromBytes(contentBytes));

            String url = BUCKET_URL_PATH + userKey;
            return url;
        }
    }

    public String uploadPhotoFromParticipation(ParticipationDto participationMvcRequestDto,
                                               String username,
                                               String contestName,
                                               MultipartFile multipartFile) throws IOException {
        String title = participationMvcRequestDto.getTitle();

        String fileExtension = checkIfFileIsPhoto(multipartFile);

        return uploadPhotoForParticipation(title, username, contestName, fileExtension, multipartFile.getInputStream());
    }

    public String photoFromCreateContest(String title, String username, String contestName, MultipartFile multipartFile)
            throws IOException {

        String fileExtension = checkIfFileIsPhoto(multipartFile);

        return uploadPhotoForParticipation(title, username, contestName, fileExtension, multipartFile.getInputStream());
    }

    public String uploadPhotoForParticipation(String title, String username,
                                              String contestName,
                                              String fileExtension,
                                              InputStream inputStream) throws IOException {
        AwsBasicCredentials awsCredentials = AwsBasicCredentials.create(ACCESS_KEY_ID, SECRET_ACCESS_KEY);
        try (S3Client s3Client = S3Client
                .builder().region(region)
                .credentialsProvider(StaticCredentialsProvider
                        .create(awsCredentials))
                .build()) {

            byte[] contentBytes = IoUtils.toByteArray(inputStream);
            long contentLength = contentBytes.length;
            String contentType = getContentType(fileExtension);

            String userKey = USERS_PATH + username + "/" + title + "_" + username + fileExtension;

            s3Client.putObject(PutObjectRequest.builder()
                            .bucket(BUCKET)
                            .key(userKey)
                            .acl(ObjectCannedACL.PUBLIC_READ)
                            .contentType(contentType)
                            .contentLength(contentLength)
                            .build(),
                    RequestBody.fromBytes(contentBytes));


            String contestKey = CONTESTS_PATH + contestName + "/" + title + "_" + username + fileExtension;

            s3Client.putObject(PutObjectRequest.builder()
                            .bucket(BUCKET)
                            .key(contestKey)
                            .acl(ObjectCannedACL.PUBLIC_READ)
                            .contentType(contentType)
                            .contentLength(contentLength)
                            .build(),
                    RequestBody.fromBytes(contentBytes));

            String url = BUCKET_URL_PATH + contestKey;

            return url;
        }
    }

    public List<String> getAllImageUrlsForAContestOrUser(String folderName, String subfolderName)
            throws SdkClientException {
        AwsBasicCredentials awsBasicCredentials = AwsBasicCredentials.create(ACCESS_KEY_ID, SECRET_ACCESS_KEY);

        try (S3Client client = S3Client
                .builder()
                .region(region)
                .credentialsProvider(StaticCredentialsProvider
                        .create(awsBasicCredentials))
                .build()) {

            List<String> urls = new ArrayList<>();
            ListObjectsRequest listObjects = ListObjectsRequest.builder()
                    .bucket(BUCKET)
                    .prefix(folderName + "/" + subfolderName + "/")
                    .build();

            ListObjectsResponse response = client.listObjects(listObjects);

            for (S3Object content : response.contents()) {
                String objectKey = content.key();
                String url = BUCKET_URL_PATH + objectKey;
                urls.add(url);
            }
            return urls;
        }
    }

    public List<String> getAllPhotosForContestCover(String folderName) {
        AwsBasicCredentials awsBasicCredentials = AwsBasicCredentials.create(ACCESS_KEY_ID, SECRET_ACCESS_KEY);
        try (S3Client client = S3Client.builder()
                .region(region)
                .credentialsProvider(StaticCredentialsProvider.create(awsBasicCredentials))
                .build()) {

            List<String> urls = new ArrayList<>();

            ListObjectsRequest listObjects = ListObjectsRequest.builder()
                    .bucket(BUCKET)
                    .prefix(folderName + "/")
                    .build();

            ListObjectsResponse response = client.listObjects(listObjects);

            for (S3Object content : response.contents()) {
                String objectKey = content.key();
                String url = BUCKET_URL_PATH + objectKey;
                urls.add(url);
            }
            return urls;
        }
    }

    private String checkIfFileIsPhoto(MultipartFile file) {
        String fileName = file.getOriginalFilename();
        String fileExtension = fileName.substring(fileName.lastIndexOf(".")).toLowerCase();
        if (!ALLOWED_EXTENSIONS.contains(fileExtension)) {
            throw new IllegalArgumentException(PHOTO_FORMAT_ERROR_MESSAGE);
        }
        return fileExtension;
    }

    private String getContentType(String fileExtension) throws FileUploadException {
        switch (fileExtension.toLowerCase()) {
            case ".jpeg":
            case ".jpg":
                return "image/jpeg";
            case ".png":
                return "image/png";
            default:
                throw new IllegalArgumentException(UNSUPPORTED_FILE_TYPE_ERROR_MESSAGE + fileExtension);
        }
    }

    public Page<String> convertListToPage(List<String> list, int pageNumber, int pageSize) {
        int start = pageNumber * pageSize;
        int end = Math.min((start + pageSize), list.size());
        return new PageImpl<>(list.subList(start, end), PageRequest.of(pageNumber, pageSize), list.size());
    }
}
