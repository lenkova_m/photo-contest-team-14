package com.company.web.photo_contest.helpers;

import com.company.web.photo_contest.models.Evaluation;
import com.company.web.photo_contest.models.Juror;
import com.company.web.photo_contest.models.Participation;
import com.company.web.photo_contest.models.User;
import com.company.web.photo_contest.modelsDto.EvaluationDto;
import com.company.web.photo_contest.modelsDto.EvaluationResponseDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@AllArgsConstructor
@Component
public class EvaluationMapper {

    public Evaluation fromDto(Participation participation, EvaluationDto evaluationDto, Juror juror) {
        Evaluation evaluation = new Evaluation();
        evaluation.setParticipation(participation);
        evaluation.setJuror(juror);
        evaluation.setRating(evaluationDto.getRating());
        evaluation.setComment(evaluationDto.getComment());
        evaluation.setDateCreated(LocalDateTime.now());
        return evaluation;
    }

    public EvaluationResponseDto convertToDto(Participation participation, Evaluation evaluation, User juror) {
        EvaluationResponseDto evaluationResponseDto = new EvaluationResponseDto();
        evaluationResponseDto.setJuror(juror);
        evaluationResponseDto.setParticipation(participation);
        evaluationResponseDto.setComment(evaluation.getComment());
        evaluationResponseDto.setRating(evaluation.getRating());
        evaluationResponseDto.setDateCreated(evaluation.getDateCreated());
        return evaluationResponseDto;
    }
}
