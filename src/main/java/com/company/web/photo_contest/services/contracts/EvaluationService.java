package com.company.web.photo_contest.services.contracts;

import com.company.web.photo_contest.models.Evaluation;
import com.company.web.photo_contest.modelsDto.EvaluationDto;
import com.company.web.photo_contest.modelsDto.EvaluationResponseDto;

import java.util.List;
import java.util.UUID;

public interface EvaluationService {

    List<Evaluation> getAll();

    List<EvaluationResponseDto> getAllEvaluationDtoByParticipation(UUID participationUuid);

    Evaluation getById(int evaluationId);

    void create(EvaluationDto evaluationDto, String username);

    void createDefaultEvaluation(UUID participationUuid);
}
