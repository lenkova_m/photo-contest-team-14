package com.company.web.photo_contest.services;

import com.company.web.photo_contest.helpers.UserMapper;
import com.company.web.photo_contest.models.Role;
import com.company.web.photo_contest.modelsDto.RoleDto;
import com.company.web.photo_contest.repositories.RoleRepository;
import com.company.web.photo_contest.services.contracts.RoleService;
import jakarta.persistence.EntityExistsException;
import jakarta.persistence.EntityNotFoundException;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class RoleServiceImpl implements RoleService {

    private final RoleRepository roleRepository;
    private final UserMapper userMapper;

    @Override
    public List<Role> getAll() {
        return roleRepository.findAll();
    }

    @Override
    public Role getRoleByName(String roleName) {
        Role role = roleRepository
                .findRoleByName(roleName)
                .orElseThrow(() -> new EntityNotFoundException("Role with this name does not exists."));
        return role;
    }

    @Override
    @Transactional
    public Role createRole(RoleDto roleDto) {
        Role roleToCreate = userMapper.convertToRoleFromCreateDto(roleDto);
        if (roleRepository.findRoleByName(roleToCreate.getName()).isPresent()) {
            throw new EntityExistsException("Role with this name already exists.");
        }
        return roleRepository.save(roleToCreate);
    }

    @Override
    @Transactional
    public Role updateRole(int roleId, RoleDto roleDto) {
        Role roleToUpdate = userMapper.convertToRoleFromUpdateDto(roleId, roleDto);
        Optional<Role> existingRoleOptional = roleRepository.findRoleByName(roleToUpdate.getName());

        if (existingRoleOptional.isPresent() && existingRoleOptional.get().getRoleId() != roleToUpdate.getRoleId()) {
            throw new EntityExistsException("Role with this name already exists.");
        }
        return roleRepository.save(roleToUpdate);
    }

    @Override
    @Transactional
    public void deleteURole(int roleId) {
        roleRepository.deleteById(roleId);
    }

}
