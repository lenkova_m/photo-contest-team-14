package com.company.web.photo_contest.services;

import com.company.web.photo_contest.models.Category;
import com.company.web.photo_contest.repositories.CategoryRepository;
import com.company.web.photo_contest.services.contracts.CategoryService;
import jakarta.persistence.EntityExistsException;
import jakarta.persistence.EntityNotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class CategoryServiceImpl implements CategoryService {

    private final CategoryRepository categoryRepository;

    @Override
    public List<Category> getAll() {
        return categoryRepository.findAll();
    }

    @Override
    public Category getCategoryById(Integer id) {
        Category category = categoryRepository.getCategoryByCategoryId(id)
                .orElseThrow(() -> new EntityNotFoundException("Category not found."));
        return category;
    }

    @Override
    public Category getCategoryByName(String categoryName) {
        Category category = categoryRepository
                .getCategoryByName(categoryName)
                .orElseThrow(() -> new EntityNotFoundException("Category not found."));
        return category;
    }

    @Override
    public List<Category> findAllByName(String name) {
        if (categoryRepository.findAllByName(name).isPresent()) {
            return categoryRepository.findAllByName(name).get();
        } else {
            return new ArrayList<>();
        }
    }

    @Override
    public List<Category> filterByName(String name) {
        return categoryRepository.filterByName(name);
    }

    @Override
    @Transactional
    public Category createCategory(Category category) {
        if (categoryRepository.getCategoryByName(category.getName()).isEmpty()) {

            return categoryRepository.save(category);
        } else {
            throw new EntityExistsException("Category already exists.");
        }
    }

    @Override
    @Transactional
    public void deleteCategory(Category category) {
        categoryRepository.delete(category);
    }

}
