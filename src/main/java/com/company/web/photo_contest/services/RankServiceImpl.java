package com.company.web.photo_contest.services;

import com.company.web.photo_contest.helpers.UserMapper;
import com.company.web.photo_contest.models.Rank;
import com.company.web.photo_contest.modelsDto.RankDto;
import com.company.web.photo_contest.repositories.RankRepository;
import com.company.web.photo_contest.services.contracts.RankService;
import jakarta.persistence.EntityExistsException;
import jakarta.persistence.EntityNotFoundException;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class RankServiceImpl implements RankService {

    private final RankRepository rankRepository;
    private final UserMapper userMapper;

    @Override
    public List<Rank> getAll() {
        return rankRepository.findAll();
    }

    @Override
    public Rank getRankByName(String roleName) {
        Rank rank = rankRepository
                .findRankByName(roleName)
                .orElseThrow(() -> new EntityNotFoundException("Rank with this name does not exists."));
        return rank;
    }

    @Override
    public Rank getRankByPoints(int points) {
        return rankRepository.findRankByPoints(points);
    }

    @Override
    @Transactional
    public Rank createRank(RankDto rankDto) {
        Rank rankToCreate = userMapper.convertToRankFromCreateDto(rankDto);
        if (rankRepository.findRankByName(rankToCreate.getName()).isPresent()) {
            throw new EntityExistsException("Rank with this name already exists.");
        }
        return rankRepository.save(rankToCreate);
    }

    @Override
    @Transactional
    public Rank updateRank(int rankId, RankDto rankDto) {
        Rank rankToUpdate = userMapper.convertToRankFromUpdateDto(rankId, rankDto);
        Optional<Rank> existingRankOptional = rankRepository.findRankByName(rankToUpdate.getName());

        if (existingRankOptional.isPresent() && existingRankOptional.get().getRankId() != rankToUpdate.getRankId()) {
            throw new EntityExistsException("Rank with this name already exists.");
        }
        return rankRepository.save(rankToUpdate);
    }

    @Override
    @Transactional
    public void deleteRank(int rankId) {
        rankRepository.deleteById(rankId);
    }

}

