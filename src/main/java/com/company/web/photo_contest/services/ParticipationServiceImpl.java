package com.company.web.photo_contest.services;

import com.company.web.photo_contest.helpers.ParticipationMapper;
import com.company.web.photo_contest.models.*;
import com.company.web.photo_contest.modelsDto.ParticipationDto;
import com.company.web.photo_contest.repositories.*;
import com.company.web.photo_contest.services.contracts.ParticipationService;
import jakarta.persistence.EntityExistsException;
import jakarta.persistence.EntityNotFoundException;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@Service
public class ParticipationServiceImpl implements ParticipationService {

    private final ParticipationRepository participationRepository;
    private final ContestRepository contestRepository;
    private final JurorRepository jurorRepository;
    private final UserRepository userRepository;
    private final ParticipationMapper participationMapper;
    private final EvaluationRepository evaluationRepository;

    @Override
    public List<Participation> getAll() {
        return participationRepository.findAll();
    }


    @Override
    public List<Participation> getAllByContest(UUID contestUuid) {
        Contest contest = contestRepository.getContestByUuid(contestUuid)
                .orElseThrow(() -> new EntityNotFoundException("Contest not found."));
        return participationRepository.findAllByContestOrderByAppraisalDesc(contest);
    }

    @Override
    public Participation getByUuid(UUID participationUuid) {
        return participationRepository.findByUuid(participationUuid)
                .orElseThrow(() -> new EntityNotFoundException("Participation not found."));
    }

    public Page<Participation> getAllParticipationsForContestAsPage(int pageNumber, int pageSize, UUID contestUuid) {
        Contest contest = contestRepository.getContestByUuid(contestUuid)
                .orElseThrow(() -> new EntityNotFoundException("Contest not found."));
        Pageable pageable = PageRequest.of(pageNumber, pageSize);
        return participationRepository.findAllByContestOrderByDateCreatedDesc(contest, pageable);
    }

    @Override
    public Page<ParticipationDto> getMappedParticipationAsPage(Page<Participation> participations) {
        List<ParticipationDto> participationDtoList = new ArrayList<>();
        List<Evaluation> evaluationList = new ArrayList<>();

        for (Participation participation : participations.getContent()) {
            evaluationList = evaluationRepository.findAllByParticipation(participation);

            participationDtoList.add(participationMapper.viewParticipation(participation, evaluationList));
        }
        return new PageImpl<>(participationDtoList,
                participations.getPageable(),
                participations.getTotalElements());
    }

    @Override
    public List<ParticipationDto> mapParticipation(UUID contestUuid) {
        List<Participation> participations = getAllByContest(contestUuid);
        List<ParticipationDto> participationDtoList = new ArrayList<>();
        List<Evaluation> evaluationList = new ArrayList<>();

        for (Participation participation : participations) {
            evaluationList = evaluationRepository.findAllByParticipation(participation);

            participationDtoList.add(participationMapper.viewParticipation(participation, evaluationList));
        }

        return participationDtoList;
    }

    @Override
    public List<Participation> getWinners() {
        return participationRepository.findWinnersFromAllFinishedContestsLastSix();
    }

    public Page<Participation> getAllWinners(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        return participationRepository.findAllWinners(pageable);
    }

    @Override
    public List<Participation> getByTitle(String title) {
        return participationRepository.findParticipationByTitle(title)
                .orElseThrow(() -> new EntityNotFoundException
                        (String.format("Participation with title: %S not found.", title)));
    }

    @Override
    public List<Participation> getByPlacement(int placement) {
        return participationRepository.findParticipationByPlacement(placement)
                .orElseThrow(() -> new EntityNotFoundException
                        (String.format("Participation with placement: %d not found.", placement)));
    }

    @Override
    public List<Participation> getAllParticipationsForContest(UUID contestUuid) {
        return participationRepository.findAllByContestUuid(contestUuid)
                .orElseThrow(() -> new EntityNotFoundException
                        (String.format("Participation with contest: %s not found.", contestUuid)));
    }

    @Override
    @Transactional
    public void create(ParticipationDto participationDto, String username) {
        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new EntityNotFoundException("User not found"));
        Contest contest = contestRepository.getContestByUuid(participationDto.getContestUuid())
                .orElseThrow(() -> new EntityNotFoundException("Contest not found"));
        Participation participation = participationMapper.fromDto(contest, participationDto, user);
        if (participationRepository.findParticipationByUserAndContest(user, contest).isEmpty()) {
            if (jurorRepository.getJurorByContestIdAndUserId(
                    participation.getContest().getContestId(),
                    participation.getUser().getUserId()).isEmpty()) {
                participationRepository.save(participation);
            } else {
                throw new EntityExistsException("User is already juror at this contest.");
            }
        } else {
            throw new EntityExistsException("Already participating");
        }
    }

    @Override
    @Transactional
    public void delete(UUID participationUuid, String username) {
        Participation participation = participationRepository.findByUuid(participationUuid)
                .orElseThrow(() -> new EntityNotFoundException("Participation not exists"));
        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new EntityNotFoundException("User not found"));
        int contestId = participation.getContest().getContestId();
        Juror juror = jurorRepository.getJurorByContestIdAndUserId(contestId, user.getUserId())
                .orElseThrow(() -> new EntityNotFoundException("Juror not found"));
        if (juror.getUserId() == user.getUserId()) {
            participationRepository.delete(participation);
        }
    }
}
