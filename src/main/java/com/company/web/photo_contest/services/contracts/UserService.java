package com.company.web.photo_contest.services.contracts;

import com.company.web.photo_contest.models.Contest;
import com.company.web.photo_contest.models.User;
import com.company.web.photo_contest.modelsDto.UserRegistrationRequest;
import com.company.web.photo_contest.modelsDto.UserUpdateRequestDto;
import jakarta.mail.MessagingException;
import org.springframework.data.domain.Page;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.UUID;

public interface UserService {
    List<User> getAll();

    List<User> getAllByRole(String role);

    User getUserByUuid(UUID userUuid);

    User getUserByUsername(String username);

    User getUserByEmail(String email);

    List<User> findBy(String firstName,
                      String lastName,
                      String username);

    User createUser(UserRegistrationRequest userRegistrationRequest);

    User updateUser(UUID uuid, UserUpdateRequestDto userUpdateRequestDto, String username);

    void deleteUser(UUID userUuid);

    void checkEmail(UserRegistrationRequest userRegistrationRequest, String url)
            throws MessagingException, UnsupportedEncodingException;

    boolean verifyEmail(String token);

    void makeJunkieOrganizer(UUID userUuid);

    void blockUser(UUID userUuid);

    void unblockUser(UUID userUuid);

    void rewardUsersByPlacement(Contest contest);

    void addPointsToUser(UUID userUuid, int points);

    void updatePasswordResetToken(String token, String email);

    void updatePassword(String token, String newPassword);

    User getByPasswordResetToken(String token);

    User getByEmailConfirmationToken(String token);

    List<User> findUsersByRank();

    Page<User> findAllUsersByRole(int pageNumber, int pageSize);
}