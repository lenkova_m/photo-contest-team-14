package com.company.web.photo_contest.services.contracts;

import com.company.web.photo_contest.models.Participation;
import com.company.web.photo_contest.modelsDto.ParticipationDto;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.UUID;

public interface ParticipationService {
    List<Participation> getAll();

    List<Participation> getAllByContest(UUID contestUuid);

    List<Participation> getAllParticipationsForContest(UUID contestUuid);

    Page<Participation> getAllParticipationsForContestAsPage(int pageNumber, int pageSize, UUID contestUuid);

    Participation getByUuid(UUID participationUuid);

    List<ParticipationDto> mapParticipation(UUID contestUuid);

    Page<ParticipationDto> getMappedParticipationAsPage(Page<Participation> participations);

    List<Participation> getWinners();

    Page<Participation> getAllWinners(int page, int size);

    List<Participation> getByTitle(String title);

    List<Participation> getByPlacement(int placement);

    void create(ParticipationDto participationDto, String username);

    void delete(UUID participationUuid, String username);
}
