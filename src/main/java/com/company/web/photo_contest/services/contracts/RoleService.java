package com.company.web.photo_contest.services.contracts;

import com.company.web.photo_contest.models.Role;
import com.company.web.photo_contest.modelsDto.RoleDto;

import java.util.List;

public interface RoleService {
    List<Role> getAll();

    Role getRoleByName(String roleName);

    Role createRole(RoleDto roleDto);

    Role updateRole(int roleId, RoleDto roleDto);

    void deleteURole(int roleId);
}
