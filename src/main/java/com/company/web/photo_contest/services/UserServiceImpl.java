package com.company.web.photo_contest.services;

import com.company.web.photo_contest.helpers.JavaMailSenderHelper;
import com.company.web.photo_contest.helpers.UserMapper;
import com.company.web.photo_contest.models.*;
import com.company.web.photo_contest.modelsDto.UserRegistrationRequest;
import com.company.web.photo_contest.modelsDto.UserUpdateRequestDto;
import com.company.web.photo_contest.repositories.ParticipationRepository;
import com.company.web.photo_contest.repositories.RankRepository;
import com.company.web.photo_contest.repositories.RoleRepository;
import com.company.web.photo_contest.repositories.UserRepository;
import com.company.web.photo_contest.services.contracts.UserService;
import jakarta.mail.MessagingException;
import jakarta.persistence.EntityExistsException;
import jakarta.persistence.EntityNotFoundException;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {

    public static final int FIRST_PLACE_SHARED_POINTS = 40;
    public static final int FIRST_PLACE_POINTS = 50;
    public static final int SECOND_PLACE_SHARED_POINTS = 25;
    public static final int SECOND_PLACE_POINTS = 35;
    public static final int THIRD_PLACE_SHARED_POINTS = 10;
    public static final int THIRD_PLACE_POINTS = 20;
    public static final int FIRST_PLACE_DOUBLED_SCORE_POINTS = 75;
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final RankRepository rankRepository;
    private final ParticipationRepository participationRepository;
    private final UserMapper userMapper;
    private final JavaMailSenderHelper mailSenderHelper;
    private final BCryptPasswordEncoder passwordEncoder;

    @Override
    public List<User> getAll() {
        return userRepository.findAll();
    }

    public List<User> getAllByRole(String role) {
        return userRepository.findUserByRoleName(role);
    }

    @Override
    public User getUserByUuid(UUID userUuid) {
        User user = userRepository.findByUuid(userUuid)
                .orElseThrow(() -> new EntityNotFoundException("User with this UUID does not exist."));
        return user;
    }

    @Override
    public User getUserByUsername(String username) {
        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new EntityNotFoundException("User with this username does not exist."));
        return user;
    }

    @Override
    public User getUserByEmail(String email) {
        User user = userRepository.findByEmail(email)
                .orElseThrow(() -> new EntityNotFoundException("User with this email does not exist."));
        return user;
    }

    public User getByPasswordResetToken(String token) {
        User user = userRepository.findByPasswordResetToken(token)
                .orElseThrow(() -> new EntityNotFoundException("User with this token does not exist."));
        return user;
    }

    public User getByEmailConfirmationToken(String token) {
        User user = userRepository.findByEmailConfirmationToken(token)
                .orElseThrow(() -> new EntityNotFoundException("User with this token does not exist."));
        return user;
    }

    @Override
    public List<User> findUsersByRank() {
        return userRepository.findUsersByRank();
    }

    @Override
    public Page<User> findAllUsersByRole(int pageNumber, int pageSize) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize);
        return userRepository.findAllUsersWithRolePhotoJunkies(pageable);
    }

    @Override
    public List<User> findBy(String firstName,
                             String lastName,
                             String username) {
        return userRepository.findByFirstNameContainingIgnoreCaseAndLastNameContainingIgnoreCaseAndUsernameNotContainingIgnoreCase
                (firstName, lastName, username);
    }


    @Override
    @Transactional
    public User createUser(UserRegistrationRequest userRegistrationRequest) {
        Role role = roleRepository
                .findRoleByName("Photo Junkie")
                .orElseThrow(() -> new EntityNotFoundException("Role with this name does not exist."));
        Rank rank = rankRepository
                .findRankByName("Junkie")
                .orElseThrow(() -> new EntityNotFoundException("Rank with this name does not exist."));
        User userToCreate = userMapper.convertToUserFromRegistrationDto(userRegistrationRequest, role, rank);
        if (userRepository.findByUsername(userToCreate.getUsername()).isPresent()) {
            throw new EntityExistsException("User with this username already exists.");
        }
        return userRepository.save(userToCreate);
    }

    @Override
    @Transactional
    public void checkEmail(UserRegistrationRequest userRegistrationRequest, String siteUrl)
            throws MessagingException, UnsupportedEncodingException {
        if (userRepository.findByEmail(userRegistrationRequest.getEmail()).isPresent()) {
            throw new EntityExistsException("User with this email already exists.");
        }
        User createdUser = createUser(userRegistrationRequest);
        String emailConfirmationToken = UUID.randomUUID().toString();
        createdUser.setEmailConfirmationToken(emailConfirmationToken);
        userRepository.save(createdUser);
        String verifyURL = siteUrl + "/auth/verify_email?token=" + createdUser.getEmailConfirmationToken();
        mailSenderHelper.sendVerificationEmail(createdUser, verifyURL);

    }

    @Override
    @Transactional
    public boolean verifyEmail(String token) {
        User userToVerify = userRepository.findByEmailConfirmationToken(token)
                .orElseThrow(() -> new EntityNotFoundException("No such user exists with this token."));
        if (userToVerify.isEnabled()) {
            return false;
        } else {
            userToVerify.setEmailConfirmationToken(null);
            userToVerify.setEnabled(true);
            userRepository.save(userToVerify);
            return true;
        }
    }

    @Override
    @Transactional
    public User updateUser(UUID uuid, UserUpdateRequestDto userUpdateRequestDto, String username) {
        Optional<User> userToUpdate = userRepository.findByUuid(uuid);
        if (userToUpdate.isPresent()) {
            User authenticatedUser = getUserByUsername(username);
            if (userToUpdate.get().getUuid() != authenticatedUser.getUuid()) {
                throw new AccessDeniedException("Only creator can update his own profile.");
            }
            User mappedUser = userMapper.convertToUserFromUpdateDto(authenticatedUser, userUpdateRequestDto);
            return userRepository.save(mappedUser);
        }
        throw new EntityNotFoundException("User with this UUID does not exist.");
    }


    @Override
    @Transactional
    public void deleteUser(UUID userUuid) {
        userRepository.deleteByUuid(userUuid);
    }

    @Override
    @Transactional
    public void makeJunkieOrganizer(UUID userUuid) {
        Optional<Role> organizer = roleRepository.findRoleByName("Organizer");
        organizer.ifPresent(role -> userRepository.updateRole(userUuid, role));
    }

    @Override
    @Transactional
    public void blockUser(UUID userUuid) {
        userRepository.blockUser(userUuid);
    }

    @Override
    @Transactional
    public void unblockUser(UUID userUuid) {
        userRepository.unblockUser(userUuid);
    }

    @Override
    @Transactional
    public void rewardUsersByPlacement(Contest contest) {
        List<Participation> participationList = participationRepository.findAllByContestOrderByPlacement(contest);
        List<Participation> usersPlacedFirst = participationList
                .stream()
                .filter(participation -> participation.getPlacement() == 1)
                .toList();
        List<Participation> usersPlacedSecond = participationList
                .stream()
                .filter(participation -> participation.getPlacement() == 2)
                .toList();
        List<Participation> usersPlacedThird = participationList
                .stream()
                .filter(participation -> participation.getPlacement() == 3)
                .toList();
        if (usersPlacedFirst.size() > 1) {
            for (Participation participation : usersPlacedFirst) {
                addPointsToUser(participation.getUser().getUuid(), FIRST_PLACE_SHARED_POINTS);
            }
        } else if (usersPlacedFirst.size() == 1) {
            Participation winner = usersPlacedFirst.get(0);
            double firstPlaceAppraisal = winner.getAppraisal();
            double doubledRewardTargetAppraisal = firstPlaceAppraisal / 2;
            if (!usersPlacedSecond.isEmpty() && usersPlacedSecond.get(0)
                    .getAppraisal() <= doubledRewardTargetAppraisal) {
                addPointsToUser(winner.getUser().getUuid(), FIRST_PLACE_DOUBLED_SCORE_POINTS);
            } else {
                addPointsToUser(winner.getUser().getUuid(), FIRST_PLACE_POINTS);
            }
        }
        if (usersPlacedSecond.size() > 1) {
            for (Participation participation : usersPlacedSecond) {
                addPointsToUser(participation.getUser().getUuid(), SECOND_PLACE_SHARED_POINTS);
            }
        } else if (usersPlacedSecond.size() == 1) {
            addPointsToUser(usersPlacedSecond.get(0).getUser().getUuid(), SECOND_PLACE_POINTS);
        }
        if (usersPlacedThird.size() > 1) {
            for (Participation participation : usersPlacedThird) {
                addPointsToUser(participation.getUser().getUuid(), THIRD_PLACE_SHARED_POINTS);
            }
        } else if (usersPlacedThird.size() == 1) {
            addPointsToUser(usersPlacedThird.get(0).getUser().getUuid(), THIRD_PLACE_POINTS);
        }

    }

    @Override
    @Transactional
    public void addPointsToUser(UUID userUuid, int points) {
        User userToUpdate = userRepository.findByUuid(userUuid)
                .orElseThrow(() -> new EntityNotFoundException("User with this UUID does not exist."));
        int userPoints = userToUpdate.getPoints();
        int userUpdatedPoints = userPoints + points;
        Rank newRank = rankRepository.findRankByPoints(userUpdatedPoints);
        userToUpdate.setRank(newRank);
        userToUpdate.setPoints(userUpdatedPoints);
    }


    @Override
    @Transactional
    public void updatePasswordResetToken(String token, String email) {
        User user = userRepository.findByEmail(email)
                .orElseThrow(() -> new EntityNotFoundException("User with this email does not exist."));
        user.setPasswordResetToken(token);
        userRepository.save(user);
    }

    @Override
    @Transactional
    public void updatePassword(String token, String newPassword) {
        User user = userRepository.findByPasswordResetToken(token)
                .orElseThrow(() -> new EntityNotFoundException("User with this token does not exist."));
        String encodedPassword = passwordEncoder.encode(newPassword);
        user.setPassword(encodedPassword);
        user.setPasswordResetToken(null);
        userRepository.save(user);
    }
    
}
