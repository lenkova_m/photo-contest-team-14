package com.company.web.photo_contest.services;

import com.company.web.photo_contest.helpers.ContestMapper;
import com.company.web.photo_contest.models.Category;
import com.company.web.photo_contest.models.Contest;
import com.company.web.photo_contest.models.User;
import com.company.web.photo_contest.modelsDto.ContestCreateDto;
import com.company.web.photo_contest.modelsDto.ContestResponseDto;
import com.company.web.photo_contest.repositories.CategoryRepository;
import com.company.web.photo_contest.repositories.ContestRepository;
import com.company.web.photo_contest.repositories.UserRepository;
import com.company.web.photo_contest.services.contracts.ContestService;
import com.company.web.photo_contest.services.contracts.JurorService;
import jakarta.persistence.EntityExistsException;
import jakarta.persistence.EntityNotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class ContestServiceImpl implements ContestService {

    private final ContestRepository contestRepository;
    private final ContestMapper contestMapper;
    private final UserRepository userRepository;
    private final CategoryRepository categoryRepository;
    private final JurorService jurorService;

    @Override
    public List<Contest> getAll() {
        return contestRepository.findAll();
    }

    @Override
    public Contest getContestByUuid(UUID uuid) {
        Contest contest = contestRepository
                .getContestByUuid(uuid)
                .orElseThrow(
                        () -> new EntityNotFoundException("Not found contest."));
        return contest;
    }

    @Override
    public Contest getContestsByTitle(String title) {
        Contest contest = contestRepository
                .getContestsByTitle(title)
                .orElseThrow(
                        () -> new EntityNotFoundException("Not found contest by title."));

        return contest;
    }

    public ContestResponseDto mapContest(UUID uuid) {
        Contest contest = getContestByUuid(uuid);

        return contestMapper.viewContest(contest);
    }

    @Override
    public List<Contest> findContestByTitle(String title) {
        if (contestRepository.findContestByTitle(title).isPresent()) {
            return contestRepository.findContestByTitle(title).get();
        } else {
            return new ArrayList<>();
        }
    }

    @Override
    public List<Contest> findContestByCategory(String category) {
        if (contestRepository.findContestByCategoryName(category).isPresent()) {
            return contestRepository.findContestByCategoryName(category).get();
        } else {
            return new ArrayList<>();
        }
    }

    @Override
    public List<Contest> findIsFinished(Boolean isFinished) {
        if (contestRepository.findContestByIsFinished(isFinished).isPresent()) {
            return contestRepository.findContestByIsFinished(isFinished).get();
        } else {
            return new ArrayList<>();
        }
    }

    public List<Contest> getActiveContests() {
        LocalDateTime now = LocalDateTime.now();
        List<Contest> contests = contestRepository.findByPhaseOneAfter(now);

        return contests.stream()
                .filter(contest -> contest.getPhaseOne().isAfter(now))
                .collect(Collectors.toList());
    }

    @Override
    public int isFinishedCount() {
        return contestRepository.countByIsFinishedTrue();
    }

    @Override
    public List<Contest> findContestByPhase(LocalDateTime phase) {
        if (contestRepository.findContestByPhaseOne(phase).isPresent()) {
            return contestRepository.findContestByPhaseOne(phase).get();
        } else {
            return new ArrayList<>();
        }
    }

    @Override
    public List<Contest> findContestByPhaseTwo(LocalDateTime phase) {
        if (contestRepository.findContestByPhaseTwo(phase).isPresent()) {
            return contestRepository.findContestByPhaseTwo(phase).get();
        } else {
            return new ArrayList<>();
        }
    }

    @Override
    public List<Contest> findByFilter(String title, Category category, Boolean isFinished) {
        return contestRepository.filterByFilters(title, category, isFinished);
    }

    @Override
    public List<Contest> filter(String title, Category category, Boolean isFinished) {
        return contestRepository.filter(title, category, isFinished);
    }

    public Page<Contest> getContestsBetweenPhaseOneAsPage(LocalDateTime phaseOne, int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        return contestRepository.findContestByDateBetweenCreateAndPhaseOne(phaseOne, pageable);
    }

    public Page<Contest> getContestsInPhaseTwoAsPage(LocalDateTime phaseTwo, int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        return contestRepository.findContestsByDatePhaseTwo(phaseTwo, pageable);
    }

    @Override
    public List<Contest> isCompleted(LocalDateTime now) {
        if (!contestRepository.isCompleted(now).isEmpty()) {
            return contestRepository.isCompleted(now);
        } else {
            return new ArrayList<>();
        }
    }

    public Page<Contest> getCompletedContestsAsPage(LocalDateTime completed, int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        return contestRepository.findContestsByIsCompleted(completed, pageable);
    }

    public Page<Contest> getAllContestWhereUserParticipatesIn(UUID userUuid, int page, int size) {
        User userId = userRepository.findByUuid(userUuid)
                .orElseThrow(() -> new EntityNotFoundException("User not found."));
        Pageable pageable = PageRequest.of(page, size);
        return contestRepository.findAllContestsUserParticipatesIn(userId.getUserId(), pageable);
    }

    public Page<Contest> getAllContestsInPhaseTwoWhereUserIsJuror(UUID userUuid, int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        LocalDateTime now = LocalDateTime.now();
        User juror = userRepository.findByUuid(userUuid)
                .orElseThrow(() -> new EntityNotFoundException("User not found."));
        return contestRepository.findAllContestsInPhaseTwoWhereUserIsJuror(now, juror.getUserId(), pageable);
    }

    @Override
    @Transactional
    public void createContest(ContestCreateDto dto, String username) {
        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new EntityNotFoundException("User not found."));

        if (contestRepository.getContestsByTitle(dto.getTitle()).isPresent()) {
            throw new EntityExistsException("Contest already exists.");
        }


        Contest contest = contestMapper.fromDto(dto, username);
        contest.setCreatedBy(user);

        Category category = categoryRepository.getCategoryByName(dto.getCategoryName())
                .orElseThrow(() -> new EntityNotFoundException("Category not found."));

        contest.setCategory(category);
        contestRepository.save(contest);

        if (!dto.getUser().isEmpty()) {
            for (User jury : dto.getUser()) {
                jurorService.create(contest.getContestId(), jury.getUserId());
            }
        }

        List<User> getAllOrganizers = userRepository.findUserByRoleName("Organizer");
        getAllOrganizers.remove(user);

        for (User users : getAllOrganizers) {
            jurorService.create(contest.getContestId(), users.getUserId());
        }
    }

    @Override
    @Transactional
    public ContestResponseDto updateContest(UUID uuid, ContestCreateDto dto, String username) {
        Optional<Contest> contestToUpdate = contestRepository.getContestByUuid(uuid);

        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new EntityNotFoundException("User not found."));

        if (contestToUpdate.isPresent()) {

            if (!contestToUpdate.get().getCreatedBy().getUsername().equals(user.getUsername())) {
                throw new AccessDeniedException("Only creator can update contest.");
            }
            Contest contest = contestMapper.fromDto(dto, user.getUsername());
            contest.setCreatedBy(user);
            contest.setContestId(contestToUpdate.get().getContestId());
            contest.setDateCreated(contestToUpdate.get().getDateCreated());
            contest.setUuid(contestToUpdate.get().getUuid());

            contestRepository.save(contest);
            return contestMapper.viewContest(contest);

        } else {
            throw new EntityNotFoundException("Contest not found.");
        }
    }

    @Override
    @Transactional
    public void deleteContest(Contest contest) {
        List<User> getAllOrganizers = userRepository.findUserByRoleName("Organizer");
        getAllOrganizers.remove(contest.getCreatedBy());

        for (User users : getAllOrganizers) {
            jurorService.delete(jurorService.getByContestUuidAndUserUuid(contest.getUuid(), users.getUuid()));
        }

        contestRepository.deleteByUuid(contest.getUuid());
    }

    @Override
    @Scheduled(fixedRate = 5000)
    public void checkContest() {
        LocalDateTime now = LocalDateTime.now();
        List<Contest> completedContests = contestRepository.isCompleted(now);

        for (Contest contest : completedContests) {
            contestRepository.finished(contest.getUuid());
        }
    }

    @Override
    public void changeIsGraded(UUID contestUuid) {
        contestRepository.graded(contestUuid);
    }
}
