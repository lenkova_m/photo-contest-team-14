package com.company.web.photo_contest.services.contracts;

import com.company.web.photo_contest.models.Category;
import com.company.web.photo_contest.models.Contest;
import com.company.web.photo_contest.modelsDto.ContestCreateDto;
import com.company.web.photo_contest.modelsDto.ContestResponseDto;
import org.springframework.data.domain.Page;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

public interface ContestService {

    List<Contest> getAll();

    Contest getContestByUuid(UUID uuid);

    Contest getContestsByTitle(String title);

    ContestResponseDto mapContest(UUID uuid);

    List<Contest> getActiveContests();

    int isFinishedCount();

    List<Contest> findContestByTitle(String title);

    List<Contest> findContestByCategory(String category);

    List<Contest> findIsFinished(Boolean isFinished);

    List<Contest> findContestByPhase(LocalDateTime phase);

    List<Contest> findContestByPhaseTwo(LocalDateTime phase);

    List<Contest> findByFilter(String title, Category category, Boolean isFinished);

    List<Contest> filter(String title, Category category, Boolean isFinished);

    Page<Contest> getContestsBetweenPhaseOneAsPage(LocalDateTime phaseOne, int page, int size);

    Page<Contest> getContestsInPhaseTwoAsPage(LocalDateTime phaseTwo, int page, int size);

    List<Contest> isCompleted(LocalDateTime phaseTwo);

    Page<Contest> getCompletedContestsAsPage(LocalDateTime completed, int page, int size);

    Page<Contest> getAllContestWhereUserParticipatesIn(UUID userUuid, int page, int size);

    Page<Contest> getAllContestsInPhaseTwoWhereUserIsJuror(UUID userUuid, int page, int size);

    @Transactional
    void createContest(ContestCreateDto dto, String username);

    @Transactional
    ContestResponseDto updateContest(UUID uuid, ContestCreateDto dto, String username);

    @Transactional
    void deleteContest(Contest contest);

    void checkContest();

    void changeIsGraded(UUID contestUuid);
}
