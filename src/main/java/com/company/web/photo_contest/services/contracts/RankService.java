package com.company.web.photo_contest.services.contracts;

import com.company.web.photo_contest.models.Rank;
import com.company.web.photo_contest.modelsDto.RankDto;

import java.util.List;

public interface RankService {
    List<Rank> getAll();

    Rank getRankByName(String roleName);

    Rank getRankByPoints(int points);

    Rank createRank(RankDto rankDto);

    Rank updateRank(int rankId, RankDto rankDto);

    void deleteRank(int rankId);
}
