package com.company.web.photo_contest.models;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.type.SqlTypes;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "participations")
public class Participation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "participation_id")
    private int participationId;

    @Column(name = "title")
    private String title;

    @Column(name = "story")
    private String story;

    @Column(name = "date_created")
    private LocalDateTime dateCreated;

    @ManyToOne
    @JoinColumn(name = "contest_id")
    private Contest contest;

    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "appraisal")
    private double appraisal;

    @Column(name = "placement")
    private int placement;

    @Column(name = "photo")
    private String photo;

    @Column(columnDefinition = "char(36)", name = "uuid")
    @JdbcTypeCode(SqlTypes.CHAR)
    private UUID uuid = UUID.randomUUID();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Participation that = (Participation) o;
        return participationId == that.participationId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(participationId);
    }
}

