package com.company.web.photo_contest.models;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.type.SqlTypes;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "contests")
public class Contest {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "contest_id")
    private int contestId;

    @Column(name = "title")
    private String title;

    @OneToOne
    @JoinColumn(name = "category_id")
    private Category category;

    @CreationTimestamp
    @Column(name = "date_created")
    private LocalDateTime dateCreated;

    @Column(name = "phase_one")
    private LocalDateTime phaseOne;

    @Column(name = "phase_two")
    private LocalDateTime phaseTwo;

    @Column(name = "is_finished")
    private boolean isFinished;

    @OneToMany
    @JoinColumn(name = "user_id")
    private Set<User> participants;

    @Column(name = "is_graded")
    private boolean isGraded;

    @Column(name = "cover_photo")
    private String coverPhoto;

    @Column(columnDefinition = "char(36)", name = "uuid")
    @JdbcTypeCode(SqlTypes.CHAR)
    private UUID uuid = UUID.randomUUID();

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User createdBy;

    public Contest(UUID uuid, String title, Category category,
                   LocalDateTime dateCreated,
                   LocalDateTime phaseOne,
                   LocalDateTime phaseTwo,
                   String coverPhoto) {
        this.uuid = uuid;
        this.title = title;
        this.category = category;
        this.dateCreated = dateCreated;
        this.phaseOne = phaseOne;
        this.phaseTwo = phaseTwo;
        this.coverPhoto = coverPhoto;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Contest contest = (Contest) o;
        return contestId == contest.contestId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(contestId);
    }
}
