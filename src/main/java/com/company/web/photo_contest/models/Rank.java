package com.company.web.photo_contest.models;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "ranks")
public class Rank {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "rank_id")
    private int rankId;

    @Column(name = "name")
    private String name;

    @Column(name = "points")
    private int points;

    public Rank(String name, int points) {
        this.name = name;
        this.points = points;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Rank rank = (Rank) o;
        return rankId == rank.rankId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(rankId);
    }
}
