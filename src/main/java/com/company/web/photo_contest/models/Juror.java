package com.company.web.photo_contest.models;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Objects;


@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "jurors")
public class Juror {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "juror_id")
    private int jurorId;
    @Column(name = "user_id")
    private int userId;
    @Column(name = "contest_id")
    private int contestId;

    public Juror(int userId, int contestId) {
        this.userId = userId;
        this.contestId = contestId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Juror juror = (Juror) o;
        return jurorId == juror.jurorId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(jurorId);
    }
}

