package com.company.web.photo_contest.security;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class CustomAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler {

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception)
            throws ServletException, IOException {
        String errorMessage = "";
        if (exception instanceof BadCredentialsException) {
            errorMessage = "bad_credentials";
        } else if (exception instanceof InternalAuthenticationServiceException) {
            errorMessage = "account_disabled";
        }
        super.setDefaultFailureUrl("/auth/login?error=" + errorMessage);
        super.onAuthenticationFailure(request, response, exception);
    }
}
