package com.company.web.photo_contest.security;

import com.company.web.photo_contest.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class CustomUserDetailsService implements UserDetailsService {
    private final UserRepository userRepository;

    @Autowired
    public CustomUserDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        com.company.web.photo_contest.models.User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("Invalid username or password"));
        if (user.isEnabled()) {
            User authUser = new User(
                    user.getUsername(),
                    user.getPassword(),
                    user.getAuthorities());
            return authUser;
        } else {
            throw new DisabledException("Please verify your email");
        }
    }
}
//    @Override
//    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//        Optional<com.company.web.photo_contest.models.User> user = userRepository.findByUsername(username);
//        if (user.isPresent()) {
//            User authUser = new User(
//                    user.get().getUsername(),
//                    user.get().getPassword(),
//                    user.get().getAuthorities());
//            return authUser;
//        } else {
//            throw new UsernameNotFoundException("Invalid username or password");
//        }
//    }
//}
