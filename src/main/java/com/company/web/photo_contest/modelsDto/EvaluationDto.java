package com.company.web.photo_contest.modelsDto;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.PositiveOrZero;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class EvaluationDto {

    private UUID participationUuid;

    @NotEmpty
    private String comment;

    @PositiveOrZero(message = "Minimum rating is 1. Rating can be 0 if the category is wrong.")
    @Max(value = 10, message = "Maximum rating is 10")
    private int rating;

    private boolean isFalseCategory;
}
