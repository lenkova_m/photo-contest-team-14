package com.company.web.photo_contest.modelsDto;

import jakarta.validation.constraints.NotEmpty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class RoleDto {
    @NotEmpty(message = "Role name can't be empty")
    private String roleName;
}
