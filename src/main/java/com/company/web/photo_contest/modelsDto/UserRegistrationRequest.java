package com.company.web.photo_contest.modelsDto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class UserRegistrationRequest {

    @NotNull(message = "User's first name can't be blank.")
    @Size(min = 4, max = 32, message = "First name must be between 4 and 32 characters long.")
    private String firstName;

    @NotNull(message = "User's last name can't be blank.")
    @Size(min = 4, max = 32, message = "Last name must be between 4 and 32 characters long.")
    private String lastName;

    @Size(min = 4, max = 32, message = "Username must be between 4 and 32 characters long.")
    private String username;

    @Size(min = 4, max = 32, message = "Password must be between 4 and 32 characters long.")
    private String password;

    @NotEmpty(message = "Email can't be blank.")
    @Email
    private String email;
}
