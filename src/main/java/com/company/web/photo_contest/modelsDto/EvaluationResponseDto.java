package com.company.web.photo_contest.modelsDto;

import com.company.web.photo_contest.models.Participation;
import com.company.web.photo_contest.models.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
public class EvaluationResponseDto {

    private String comment;
    private int rating;
    private LocalDateTime dateCreated;
    private Participation participation;
    private User juror;
}
