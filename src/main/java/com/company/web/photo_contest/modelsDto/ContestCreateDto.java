package com.company.web.photo_contest.modelsDto;

import com.company.web.photo_contest.models.User;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
public class ContestCreateDto {

    @NotEmpty(message = "Title is required.")
    private String title;

    @NotEmpty(message = "Category is required.")
    private String categoryName;

    @NotNull(message = "Date and time is required.")
    private LocalDateTime phaseOne;

    @NotNull(message = "Date and time is required.")
    private LocalDateTime phaseTwo;

    private MultipartFile photoFile;

    private String selectCoverPhoto;

    private String coverPhotoUrl;

    private List<User> user;
}
