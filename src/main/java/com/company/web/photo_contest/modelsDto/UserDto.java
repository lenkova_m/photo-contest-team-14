package com.company.web.photo_contest.modelsDto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class UserDto {

    private String firstName;

    private String password;

    private String lastName;

    private String username;

    private String email;

    private String role;

    private String rank;

    private int points;

    private String profilePhoto;

    private UUID userUuid;
}
