package com.company.web.photo_contest.modelsDto;

import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class RankPointsUpdateRequestDto {
    @NotNull(message = "Points must be greater than 0")
    private int points;
}
