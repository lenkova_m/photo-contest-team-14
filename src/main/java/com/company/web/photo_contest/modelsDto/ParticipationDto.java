package com.company.web.photo_contest.modelsDto;

import com.company.web.photo_contest.models.User;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ParticipationDto {

    @NotEmpty
    @Pattern(regexp = "^[a-zA-Z0-9 ]+$", message = "Title must contain only alphanumeric characters and spaces")
    private String title;
    @NotEmpty
    private String story;

    private UUID contestUuid;

    private User user;

    private String photo;

    private MultipartFile photoFile;

    private UUID participationUuid;

    private int commentsCount;

    private double appraisal;

}
