package com.company.web.photo_contest.modelsDto;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class RankDto {
    @NotEmpty(message = "Rank name can't be empty")
    private String rankName;
    @NotNull(message = "Required points must be greater than 0")
    private int points;
}
