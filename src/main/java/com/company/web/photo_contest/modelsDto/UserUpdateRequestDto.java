package com.company.web.photo_contest.modelsDto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class UserUpdateRequestDto {

    private UUID userUuid;
    @NotNull(message = "First Name can't be blank.")
    @Size(min = 4, max = 32, message = "First Name must be between 4 and 32 characters long.")
    private String firstName;

    @NotNull(message = "Last Name can't be blank.")
    @Size(min = 4, max = 32, message = "Last Name must be between 4 and 32 characters long.")
    private String lastName;

    @NotNull(message = "Password can't be blank.")
    @NotBlank(message = "Password can't be blank.")
    private String password;

    private String profilePhoto;

    private MultipartFile photoFile;

    private String username;

    private String email;

    private String role;

    private String rank;

    private int points;
}
