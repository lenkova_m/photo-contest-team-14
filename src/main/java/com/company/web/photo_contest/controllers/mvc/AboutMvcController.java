package com.company.web.photo_contest.controllers.mvc;

import com.company.web.photo_contest.services.contracts.UserService;
import lombok.AllArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@AllArgsConstructor
@Controller
@RequestMapping("/about")
public class AboutMvcController {

    private final UserService userService;

    @GetMapping
    public String showAboutPage(Model model,
                                Authentication authentication) {
        if (authentication != null) {
            model.addAttribute("user", userService.getUserByUsername(authentication.getName()));
        } else {
            model.addAttribute("user", null);
        }
        return "about";
    }
}
