package com.company.web.photo_contest.controllers.mvc;

import com.company.web.photo_contest.helpers.JavaMailSenderHelper;
import com.company.web.photo_contest.modelsDto.UserRegistrationRequest;
import com.company.web.photo_contest.services.contracts.UserService;
import jakarta.mail.MessagingException;
import jakarta.persistence.EntityExistsException;
import jakarta.persistence.EntityNotFoundException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.UnsupportedEncodingException;

@Controller
@RequestMapping("/auth")
@AllArgsConstructor
public class AuthMvcController {
    public static final String VERIFY_SUCCESS_MESSAGE_ONE = "Congratulations, your account has been verified!";
    public static final String VERIFY_SUCCESS_MESSAGE_TWO = "You can now log in and explore our vast majority of contests.";
    public static final String VERIFY_FAILED_MESSAGE = "Your email verification token could be invalid or your account could be already verified.";
    private final UserService userService;
    private final JavaMailSenderHelper mailSenderHelper;

    @GetMapping("/login")
    public String showLoginPage() {
        return "login";
    }

    @GetMapping("/register")
    public String registerPage(Model model) {
        UserRegistrationRequest userRegistrationRequest = new UserRegistrationRequest();
        model.addAttribute("userReg", userRegistrationRequest);
        return "registration";
    }

    @PostMapping("/register")
    public String register(@Valid @ModelAttribute("userReg") UserRegistrationRequest userRegistrationRequest,
                           BindingResult bindingResult,
                           HttpServletRequest request) {
        if (bindingResult.hasErrors()) {
            return "registration";
        }
        try {
            userService.checkEmail(userRegistrationRequest, mailSenderHelper.getSiteURL(request));
            return "redirect:/auth/login?success";
        } catch (EntityExistsException e) {
            if (e.getMessage().contains("email")) {
                bindingResult.rejectValue("email", "email_error", e.getMessage());
                return "redirect:/auth/register?fail";
            }
            bindingResult.rejectValue("username", "username_error", e.getMessage());
            return "redirect:/auth/register?fail";
        } catch (EntityNotFoundException e) {
            if (e.getMessage().contains("role")) {
                bindingResult.rejectValue("role", "role_error", e.getMessage());
                return "redirect:/auth/register?fail";
            }
            bindingResult.rejectValue("rank", "rank_error", e.getMessage());
            return "redirect:/auth/register?fail";
        } catch (MessagingException | UnsupportedEncodingException e) {
            return "redirect:/auth/register?fail";
        }
    }

    @GetMapping("/verify_email")
    public String showVerifyEmailPage(@Param("token") String token, Model model) {
        try {
            if (userService.verifyEmail(token)) {
                model.addAttribute("firstMessage", VERIFY_SUCCESS_MESSAGE_ONE);
                model.addAttribute("secondMessage", VERIFY_SUCCESS_MESSAGE_TWO);
            } else {
                model.addAttribute("message", VERIFY_FAILED_MESSAGE);
            }
            return "verify-message-page";
        } catch (EntityNotFoundException e) {
            model.addAttribute("message", e.getMessage());
            return "verify-message-page";
        }
    }
}
