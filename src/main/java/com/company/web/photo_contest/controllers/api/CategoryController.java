package com.company.web.photo_contest.controllers.api;

import com.company.web.photo_contest.models.Category;
import com.company.web.photo_contest.services.contracts.CategoryService;
import jakarta.persistence.EntityExistsException;
import jakarta.persistence.EntityNotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/api/category")
@PreAuthorize("hasRole('ROLE_Organizer')")
public class CategoryController {

    private final CategoryService categoryService;

    @GetMapping
    public List<Category> getAllCategories() {
        List<Category> categories;
        categories = categoryService.getAll();
        return categories;
    }

    @GetMapping("/{categoryId}")
    public Category getCategoryById(@PathVariable Integer categoryId) {
        try {
            return categoryService.getCategoryById(categoryId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/category-name/{categoryName}")
    public Category getCategoryByName(@PathVariable String categoryName) {
        try {
            return categoryService.getCategoryByName(categoryName);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/name/{categoryName}")
    public List<Category> findByName(@PathVariable String categoryName) {
        List<Category> categories;
        categories = categoryService.findAllByName(categoryName);
        return categories;
    }

    @GetMapping("/filter")
    public List<Category> filter(@RequestParam(required = false) String name) {
        return categoryService.filterByName(name);
    }

    @PostMapping()
    public Category createCategory(@RequestBody Category category) {
        try {
            return categoryService.createCategory(category);
        } catch (EntityExistsException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void deleteCategory(@PathVariable int id) {
        Category category = getCategoryById(id);
        try {
            categoryService.deleteCategory(category);
        } catch (EntityExistsException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

}
