package com.company.web.photo_contest.controllers.mvc;

import com.company.web.photo_contest.helpers.JavaMailSenderHelper;
import com.company.web.photo_contest.models.User;
import com.company.web.photo_contest.services.contracts.UserService;
import jakarta.mail.MessagingException;
import jakarta.persistence.EntityNotFoundException;
import jakarta.servlet.http.HttpServletRequest;
import lombok.AllArgsConstructor;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.UnsupportedEncodingException;
import java.util.UUID;

@Controller
@AllArgsConstructor
@RequestMapping("/")
public class ForgotPasswordController {

    public static final String LINK_SENT_MESSAGE = "We have sent a reset password link to your email.";
    public static final String EMAIL_SEND_ERROR_MESSAGE = "There was an error while sending the email. Please try again.";
    public static final String INVALID_TOKEN = "Invalid Token";
    public static final String PASSWORD_RESET_SUCCESS_MESSAGE = "You have successfully changed your password.";
    private final JavaMailSenderHelper mailSenderHelper;
    private final UserService userService;

    @GetMapping("/forgot-password")
    public String showForgotPasswordForm() {
        return "forgot-password-form";
    }

    @PostMapping("/forgot-password")
    public String processForgotPassword(HttpServletRequest request, Model model) {
        try {
            String email = request.getParameter("email");
            String token = UUID.randomUUID().toString();
            userService.updatePasswordResetToken(token, email);
            String resetPasswordLink = mailSenderHelper.getSiteURL(request) + "/reset-password?token=" + token;
            mailSenderHelper.sendResetPasswordEmail(email, resetPasswordLink);
            model.addAttribute("message", LINK_SENT_MESSAGE);
            return "reset-password-message-page";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
        } catch (UnsupportedEncodingException | MessagingException e) {
            model.addAttribute("error", EMAIL_SEND_ERROR_MESSAGE);
        }
        return "forgot-password-form";
    }

    @GetMapping("/reset-password")
    public String showResetPasswordForm(@Param(value = "token") String token, Model model) {
        try {
            User user = userService.getByPasswordResetToken(token);
            model.addAttribute("token", token);
            return "reset-password-form";
        } catch (EntityNotFoundException e) {
            model.addAttribute("message", INVALID_TOKEN);
            return "reset-password-message-page";
        }
    }

    @PostMapping("/reset-password")
    public String processResetPassword(HttpServletRequest request, Model model) {
        try {
            String token = request.getParameter("token");
            String password = request.getParameter("password");
            userService.updatePassword(token, password);
            model.addAttribute("message", PASSWORD_RESET_SUCCESS_MESSAGE);
            return "reset-password-message-page";
        } catch (EntityNotFoundException e) {
            model.addAttribute("message", INVALID_TOKEN);
            return "reset-password-message-page";
        }
    }

}
