package com.company.web.photo_contest.controllers.mvc;

import com.company.web.photo_contest.helpers.AmazonS3UploadUtil;
import com.company.web.photo_contest.models.Category;
import com.company.web.photo_contest.models.Contest;
import com.company.web.photo_contest.models.Participation;
import com.company.web.photo_contest.models.User;
import com.company.web.photo_contest.modelsDto.ContestCreateDto;
import com.company.web.photo_contest.modelsDto.ContestResponseDto;
import com.company.web.photo_contest.modelsDto.ParticipationDto;
import com.company.web.photo_contest.security.SecurityUtil;
import com.company.web.photo_contest.services.contracts.*;
import jakarta.persistence.EntityExistsException;
import jakarta.persistence.EntityNotFoundException;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping("/")
@AllArgsConstructor
public class ContestMvcController {

    private final ContestService contestService;
    private final ParticipationService participationService;
    private final UserService userService;
    private final AmazonS3UploadUtil amazonS3UploadUtil;
    private final CategoryService categoryService;
    private final JurorService jurorService;

    @ModelAttribute("allContests")
    public List<Contest> populateContests() {
        return contestService.getAll();
    }

    @ModelAttribute("allCategory")
    public List<Category> populateCategory() {
        return categoryService.getAll();
    }

    @ModelAttribute("user")
    public User getCurrentUser(Authentication authentication) {
        return userService.getUserByUsername(authentication.getName());
    }

    @ModelAttribute("isCurrentUserJuror")
    public boolean isCurrentUserJuror(Authentication authentication) {
        return jurorService.findIfUserIsJury(authentication.getName());
    }

    @GetMapping("contest/single-contest")
    public String showSingleContest(@RequestParam UUID contestUuid,
                                    @RequestParam(defaultValue = "0") int page,
                                    @RequestParam(defaultValue = "6") int size,
                                    Model model) {
        try {
            User user = new User();
            String username = SecurityUtil.getSessionUser();
            if (username != null) {
                user = userService.getUserByUsername(username);
                model.addAttribute("user", user);
            }
            model.addAttribute("user", user);

            ContestResponseDto contestResponseDto = contestService.mapContest(contestUuid);
            Page<Participation> list = participationService.getAllParticipationsForContestAsPage(page, size, contestUuid);
            Page<ParticipationDto> participationDtoPage = participationService.getMappedParticipationAsPage(list);
            LocalDateTime now = LocalDateTime.now();

            model.addAttribute("contest", contestResponseDto);
            model.addAttribute("participation", participationDtoPage);
            model.addAttribute("now", now);

            return "single-contest";
        } catch (EntityNotFoundException e) {
            return "404";
        }
    }

    @GetMapping("contest/delete")
    public String deleteContest(@RequestParam UUID contestUuid) {
        try {
            Contest contest = contestService.getContestByUuid(contestUuid);
            contestService.deleteContest(contest);
            return "redirect:/";
        } catch (EntityNotFoundException e) {
            return "404";
        } catch (EntityExistsException e) {
            return "login";
        }
    }

    @PreAuthorize("hasRole('ROLE_Organizer')")
    @GetMapping("/contest/new")
    public String showNewContestPage(Model model) {
        try {
            User user = new User();
            String username = SecurityUtil.getSessionUser();
            List<User> allUser = userService.findUsersByRank();
            LocalDateTime dateTime = LocalDateTime.now();
            LocalDateTime plusMonths = dateTime.plusMonths(1);
            if (username != null) {
                user = userService.getUserByUsername(username);
                List<String> photoUrl = amazonS3UploadUtil.getAllPhotosForContestCover("contests");

                model.addAttribute("listOfPhotos", photoUrl);
                model.addAttribute("user", user);
                model.addAttribute("juror", allUser);
                model.addAttribute("today", plusMonths);
            } else {
                return "redirect:/auth/login?success";
            }

        } catch (EntityExistsException | EntityNotFoundException e) {
            return "404";
        }

        model.addAttribute("contest", new ContestCreateDto());
        return "create-contest";
    }

    @PreAuthorize("hasRole('ROLE_Organizer')")
    @PostMapping("/contest/new")
    public String createNewContest(@Valid @ModelAttribute("contest")
                                   @RequestBody ContestCreateDto contestCreateDto,
                                   BindingResult bindingResult, Model model) {

        try {
            User user;
            String username = SecurityUtil.getSessionUser();

            if (username != null) {
                user = userService.getUserByUsername(username);
                model.addAttribute("user", user);
            } else {
                return "redirect:/auth/login?success";
            }

            if (bindingResult.hasErrors()) {
                return "create-contest";
            }

            contestService.createContest(contestCreateDto, username);
            return "redirect:/";


        } catch (EntityNotFoundException | EntityExistsException | IllegalArgumentException e) {
            model.addAttribute("error", e.getMessage());
            return "participation-fail";
        }
    }

    @GetMapping("/contests/phase-one")
    public String showPhaseOne(@RequestParam(defaultValue = "0") int page,
                               @RequestParam(defaultValue = "6") int size,
                               Model model) {

        LocalDateTime now = LocalDateTime.now();
        Page<Contest> contestsPage = contestService.getContestsBetweenPhaseOneAsPage(now, page, size);

        model.addAttribute("contest", contestsPage);

        return "phase-one";
    }

    @GetMapping("/contests/phase-two")
    public String showPhaseTwo(@RequestParam(defaultValue = "0") int page,
                               @RequestParam(defaultValue = "6") int size,
                               Model model) {

        LocalDateTime now = LocalDateTime.now();
        Page<Contest> contestsPage = contestService.getContestsInPhaseTwoAsPage(now, page, size);

        model.addAttribute("contest", contestsPage);

        return "phase-two";
    }

    @GetMapping("contests/finished")
    public String showFinishContest(@RequestParam(defaultValue = "0") int page,
                                    @RequestParam(defaultValue = "6") int size,
                                    Model model) {
        LocalDateTime now = LocalDateTime.now();
        contestService.checkContest();
        Page<Contest> contestsPage = contestService.getCompletedContestsAsPage(now, page, size);

        model.addAttribute("contest", contestsPage);

        return "finished";
    }

    @PostMapping("/categories/create")
    @ResponseBody
    public ResponseEntity<?> createCategory(@RequestBody Category category) {

        categoryService.createCategory(category);
        return ResponseEntity.ok().build();
    }

}
