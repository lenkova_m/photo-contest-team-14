package com.company.web.photo_contest.controllers.mvc;

import com.company.web.photo_contest.helpers.AmazonS3UploadUtil;
import com.company.web.photo_contest.helpers.UserMapper;
import com.company.web.photo_contest.models.Contest;
import com.company.web.photo_contest.models.Participation;
import com.company.web.photo_contest.models.User;
import com.company.web.photo_contest.modelsDto.UserDto;
import com.company.web.photo_contest.modelsDto.UserUpdateRequestDto;
import com.company.web.photo_contest.services.contracts.ContestService;
import com.company.web.photo_contest.services.contracts.JurorService;
import com.company.web.photo_contest.services.contracts.ParticipationService;
import com.company.web.photo_contest.services.contracts.UserService;
import jakarta.persistence.EntityNotFoundException;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping
@AllArgsConstructor
public class UserMvcController {

    private final UserService userService;
    private final ParticipationService participationService;
    private final ContestService contestService;
    private final JurorService jurorService;
    private final UserMapper userMapper;
    private final AmazonS3UploadUtil amazonS3UploadUtil;

    @ModelAttribute("allUsers")
    public List<User> getAllByRole() {
        return userService.getAllByRole("Photo Junkie");
    }

    @ModelAttribute("user")
    public User getCurrentUser(Authentication authentication) {
        return userService.getUserByUsername(authentication.getName());
    }

    @ModelAttribute("isCurrentUserJuror")
    public boolean isCurrentUserJuror(Authentication authentication) {
        return jurorService.findIfUserIsJury(authentication.getName());
    }

    @GetMapping("/users/profile")
    public String showProfilePage(@RequestParam UUID userUuid,
                                  @RequestParam(defaultValue = "0") int page,
                                  @RequestParam(defaultValue = "6") int size,
                                  Model model,
                                  Authentication authentication) {
        try {
            User currentUser = userService.getUserByUsername(authentication.getName());
            User user = userService.getUserByUuid(userUuid);
            UserDto userDto = userMapper.toDto(user);
            List<String> photoUrl = amazonS3UploadUtil.getAllImageUrlsForAContestOrUser("users", user.getUsername());
            Page<String> convertPhotoListToPages = amazonS3UploadUtil.convertListToPage(photoUrl, page, size);

            model.addAttribute("photoUrl", convertPhotoListToPages);
            model.addAttribute("userDto", userDto);
            model.addAttribute("user", currentUser);
            return "profile-view";
        } catch (EntityNotFoundException e) {
            return "404";
        }
    }

    @GetMapping("/users/profile/edit")
    public String showEditProfilePage(@RequestParam UUID userUuid, Model model, Authentication authentication) {
        try {
            User currentUser = userService.getUserByUsername(authentication.getName());
            model.addAttribute("userUuid", userUuid);
            UserUpdateRequestDto userDto = userMapper.convertToUserUpdateDtoFromUser(currentUser);
            model.addAttribute("userDto", userDto);
            model.addAttribute("user", currentUser);
            return "edit-profile";
        } catch (EntityNotFoundException e) {
            return "404";
        }
    }

    @PostMapping("/users/profile/edit")
    public String editProfile(@RequestParam UUID userUuid,
                              @RequestParam("photoFile") MultipartFile multipartFile,
                              @Valid @ModelAttribute("userDto") UserUpdateRequestDto userDto,
                              BindingResult bindingResult,
                              Model model,
                              Authentication authentication) throws IOException {

        User currentUser = userService.getUserByUsername(authentication.getName());
        if (bindingResult.hasErrors()) {
            userDto.setProfilePhoto(currentUser.getProfilePhoto());
            userDto.setUserUuid(currentUser.getUuid());
            userDto.setPoints(currentUser.getPoints());
            userDto.setEmail(currentUser.getEmail());
            userDto.setRank(currentUser.getRank().getName());
            userDto.setRole(currentUser.getRole().getName());
            userDto.setUsername(currentUser.getUsername());
            return "edit-profile";
        }

        try {
            String photoUrl = amazonS3UploadUtil.uploadPhotoFromUser(currentUser.getUsername(), multipartFile);
            if (photoUrl == null) {
                userDto.setProfilePhoto(currentUser.getProfilePhoto());
            } else {
                userDto.setProfilePhoto(photoUrl);
            }
            userDto.setUserUuid(currentUser.getUuid());
            userDto.setPoints(currentUser.getPoints());
            userDto.setEmail(currentUser.getEmail());
            userDto.setRank(currentUser.getRank().getName());
            userDto.setRole(currentUser.getRole().getName());
            userDto.setUsername(currentUser.getUsername());
            currentUser = userMapper.convertToUserFromUpdateDto(currentUser, userDto);
            userService.updateUser(currentUser.getUuid(), userDto, currentUser.getUsername());
            return "redirect:/users/profile?userUuid=" + userUuid;
        } catch (EntityNotFoundException | IOException e) {
            model.addAttribute("error", e.getMessage());
            return "404";
        }
    }

    @GetMapping("/users")
    public String showAllUsersPage(@RequestParam(defaultValue = "0") int page,
                                   @RequestParam(defaultValue = "32") int size,
                                   Model model,
                                   Authentication authentication) {
        try {
            User currentUser = userService.getUserByUsername(authentication.getName());
            model.addAttribute("user", currentUser);
            Page<User> photoJunkiesPage = userService.findAllUsersByRole(page, size);
            model.addAttribute("usersPage", photoJunkiesPage);
            return "users-page";
        } catch (EntityNotFoundException e) {
            return "404";
        }
    }

    @PreAuthorize("hasRole('ROLE_Organizer')")
    @PostMapping("users/organizer")
    public String makeOrganizer(@RequestParam UUID uuid) {
        userService.makeJunkieOrganizer(uuid);
        return "redirect:/users/profile?userUuid=" + uuid;
    }

    @GetMapping("/winners")
    public String showAllWinners(@RequestParam(defaultValue = "0") int page,
                                 @RequestParam(defaultValue = "6") int size,
                                 Model model) {
        try {
            Page<Participation> winnersList = participationService.getAllWinners(page, size);
            model.addAttribute("winnersList", winnersList);
            return "winners";
        } catch (EntityNotFoundException e) {
            return "404";
        }
    }

    @GetMapping("/users/{userUuid}/contests")
    public String showAllUsersParticipationsInContestPage(@PathVariable UUID userUuid,
                                                          @RequestParam(defaultValue = "0") int page,
                                                          @RequestParam(defaultValue = "6") int size,
                                                          Model model) {
        try {
            Page<Contest> allContestsForUser = contestService.getAllContestWhereUserParticipatesIn(userUuid, page, size);
            model.addAttribute("userUuid", userUuid);
            model.addAttribute("contestList", allContestsForUser);
            return "user-contests";
        } catch (EntityNotFoundException e) {
            return "404";
        }
    }

    @GetMapping("/users/{userUuid}/juror-contests")
    public String showAllContestsInPhaseTwoWhereUserIsJuror(@PathVariable UUID userUuid,
                                                            @RequestParam(defaultValue = "0") int page,
                                                            @RequestParam(defaultValue = "6") int size,
                                                            Model model) {
        try {
            Page<Contest> allContestsWhereUserIsJury = contestService.
                    getAllContestsInPhaseTwoWhereUserIsJuror(userUuid, page, size);
            model.addAttribute("userUuid", userUuid);
            model.addAttribute("contestList", allContestsWhereUserIsJury);
            return "juror-contests";
        } catch (EntityNotFoundException e) {
            return "404";
        }
    }
}
