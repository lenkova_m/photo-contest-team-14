package com.company.web.photo_contest.controllers.api;

import com.company.web.photo_contest.models.Participation;
import com.company.web.photo_contest.modelsDto.ParticipationDto;
import com.company.web.photo_contest.services.contracts.ParticipationService;
import jakarta.persistence.EntityExistsException;
import jakarta.persistence.EntityNotFoundException;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AuthorizationServiceException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@RestController
@RequestMapping("/api/participations/")
public class ParticipationController {

    private final ParticipationService participationService;

    @GetMapping
    public List<Participation> getAll() {
        return participationService.getAll();
    }

    @GetMapping("/contests/{contestUuid}/participations")
    public List<Participation> getAllForContest(@PathVariable UUID contestUuid) {
        return participationService.getAllByContest(contestUuid);
    }

    @GetMapping("{participationUuid}")
    public Participation getByUuid(@PathVariable UUID participationUuid) {
        try {
            return participationService.getByUuid(participationUuid);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public void create(@Valid @RequestBody ParticipationDto participationDto, Authentication authentication) {
        try {
            participationService.create(participationDto, authentication.getName());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (EntityExistsException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PreAuthorize("hasRole('ROLE_Organizer')")
    @DeleteMapping("{participationUuid}")
    public void delete(@PathVariable UUID participationUuid,
                       Authentication authentication) {
        try {
            participationService.delete(participationUuid, authentication.getName());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (AuthorizationServiceException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}
