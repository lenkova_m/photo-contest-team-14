package com.company.web.photo_contest.controllers.api;

import com.company.web.photo_contest.models.Evaluation;
import com.company.web.photo_contest.modelsDto.EvaluationDto;
import com.company.web.photo_contest.modelsDto.EvaluationResponseDto;
import com.company.web.photo_contest.services.contracts.EvaluationService;
import jakarta.persistence.EntityExistsException;
import jakarta.persistence.EntityNotFoundException;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@RestController
@RequestMapping("/api")
public class EvaluationController {

    private final EvaluationService evaluationService;

    @GetMapping("/evaluations")
    public List<Evaluation> getAll() {
        return evaluationService.getAll();
    }

    @GetMapping("/participations/{participationUuid}/evaluations")
    public List<EvaluationResponseDto> getAllByParticipation(@PathVariable UUID participationUuid) {
        try {
            return evaluationService.getAllEvaluationDtoByParticipation(participationUuid);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/evaluations/{evaluationId}")
    public Evaluation getById(@PathVariable int evaluationId) {
        try {
            return evaluationService.getById(evaluationId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("/evaluations")
    public void create(Authentication authentication, @Valid @RequestBody EvaluationDto evaluationDto) {
        try {
            evaluationService.create(evaluationDto, authentication.getName());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (EntityExistsException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

}
