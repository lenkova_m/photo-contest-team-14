package com.company.web.photo_contest.repositories;

import com.company.web.photo_contest.models.Rank;
import com.company.web.photo_contest.models.Role;
import com.company.web.photo_contest.models.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
@Transactional(readOnly = true)
public interface UserRepository extends JpaRepository<User, Integer> {

    Optional<User> findByUuid(UUID userUuid);

    Optional<User> findByUsername(String username);

    Optional<User> findByEmail(String email);

    List<User> findUserByRoleName(String role);

    Optional<User> findByEmailConfirmationToken(String token);

    Optional<User> findByPasswordResetToken(String token);

    List<User> findByFirstNameContainingIgnoreCaseAndLastNameContainingIgnoreCaseAndUsernameNotContainingIgnoreCase
            (String firstName, String lastName, String username);

    void deleteByUuid(UUID userUuid);

    @Modifying
    @Query("update User u " +
            "set u.role = :role " +
            "where u.uuid = :uuid")
    void updateRole(@Param("uuid") UUID userUuid, @Param("role") Role role);

    @Modifying
    @Query("update User u " +
            "set u.isLocked = true " +
            "where u.uuid = :uuid")
    void blockUser(@Param("uuid") UUID userUuid);

    @Modifying
    @Query("update User u " +
            "set u.isLocked = false " +
            "where u.uuid = :uuid")
    void unblockUser(@Param("uuid") UUID userUuid);

    @Modifying
    @Query("update User u " +
            "set u.points = :points " +
            "where u.uuid = :uuid")
    void updatePoints(@Param("points") int points, @Param("uuid") UUID userUuid);

    @Modifying
    @Query("update User u " +
            "set u.rank = :rank " +
            "where u.uuid = :uuid")
    void updateRank(@Param("rank") Rank rank, @Param("uuid") UUID userUuid);

    @Query("SELECT u FROM User u" +
            " WHERE u.rank.rankId > 2 and u.role.roleId > 1")
    List<User> findUsersByRank();

    @Query("select u from User u " +
            "where u.role.roleId = 2 " +
            "order by u.points desc ")
    Page<User> findAllUsersWithRolePhotoJunkies(Pageable pageable);
}
