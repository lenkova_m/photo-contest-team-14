package com.company.web.photo_contest.repositories;

import com.company.web.photo_contest.models.Evaluation;
import com.company.web.photo_contest.models.Juror;
import com.company.web.photo_contest.models.Participation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
@Transactional(readOnly = true)
public interface EvaluationRepository extends JpaRepository<Evaluation, Integer> {

    List<Evaluation> findAllByParticipation(Participation participation);

    Optional<Evaluation> findByJurorAndParticipation(Juror juror, Participation participation);

}
