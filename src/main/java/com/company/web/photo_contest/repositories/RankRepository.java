package com.company.web.photo_contest.repositories;

import com.company.web.photo_contest.models.Rank;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
@Transactional(readOnly = true)
public interface RankRepository extends JpaRepository<Rank, Integer> {

    Optional<Rank> findRankByName(String rankName);

    @Query(value = " from Rank " +
            "where points <= :insertedPoints" +
            " order by rankId desc" +
            " limit 1")
    Rank findRankByPoints(int insertedPoints);
}
