package com.company.web.photo_contest.repositories;

import com.company.web.photo_contest.models.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
@Transactional(readOnly = true)
public interface CategoryRepository extends JpaRepository<Category, Integer> {

    Optional<Category> getCategoryByCategoryId(Integer integer);

    Optional<Category> getCategoryByName(String name);

    Optional<List<Category>> findAllByName(String name);

    @Query(value = "select c from Category c"
            + " where (c.name LIKE %:name%)"
    )
    List<Category> filterByName(@Param("name") String name);
}
