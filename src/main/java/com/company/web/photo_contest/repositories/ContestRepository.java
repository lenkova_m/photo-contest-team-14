package com.company.web.photo_contest.repositories;

import com.company.web.photo_contest.models.Category;
import com.company.web.photo_contest.models.Contest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
@Transactional(readOnly = true)
public interface ContestRepository extends JpaRepository<Contest, Integer> {

    Optional<Contest> getContestByUuid(UUID uuid);

    Optional<Contest> getContestsByTitle(String title);

    @Modifying
    void deleteByUuid(UUID uuid);

    List<Contest> findByPhaseOneAfter(LocalDateTime now);

    int countByIsFinishedTrue();

    Optional<List<Contest>> findContestByTitle(String title);

    Optional<List<Contest>> findContestByCategoryName(String category);

    Optional<List<Contest>> findContestByIsFinished(Boolean isFinished);

    Optional<List<Contest>> findContestByPhaseOne(LocalDateTime localDateTime);

    Optional<List<Contest>> findContestByPhaseTwo(LocalDateTime localDateTime);

    @Query(value = "SELECT c FROM Contest c"
            + " WHERE (:title IS NULL OR c.title = :title)"
            + " AND (:category IS NULL OR c.category = :category)"
            + " AND (:type IS NULL OR c.isFinished = :type)"
    )
    List<Contest> filterByFilters(@Param("title") String title,
                                  @Param("category") Category category,
                                  @Param("type") Boolean isFinished);

    @Query(value = "SELECT c FROM Contest c"
            + " WHERE (c.title LIKE %:title%)"
            + " AND (c.category.name LIKE %:category%)"
            + " AND (:type IS NULL OR c.isFinished = :type)"
    )
    List<Contest> filter(@Param("title") String title,
                         @Param("category") Category category,
                         @Param("type") Boolean isFinished);

    @Query(value = "SELECT c FROM Contest c "
            + " WHERE :date BETWEEN c.dateCreated AND c.phaseOne")
    Page<Contest> findContestByDateBetweenCreateAndPhaseOne(LocalDateTime date, Pageable pageable);

    @Query(value = "SELECT c FROM Contest c "
            + " WHERE :date BETWEEN c.phaseOne AND c.phaseTwo")
    Page<Contest> findContestsByDatePhaseTwo(LocalDateTime date, Pageable pageable);

    @Query(value = "SELECT c FROM Contest c" +
            " WHERE :now > c.phaseTwo")
    List<Contest> isCompleted(LocalDateTime now);

    @Query(value = "SELECT c FROM Contest c" +
            " WHERE :now > c.phaseTwo")
    Page<Contest> findContestsByIsCompleted(LocalDateTime now, Pageable pageable);

    @Modifying
    @Query("UPDATE Contest c " +
            "SET c.isFinished = TRUE " +
            " WHERE c.uuid = :uuid"
    )
    void finished(@Param("uuid") UUID contestUuid);

    @Modifying
    @Query("UPDATE Contest c " +
            "SET c.isGraded = TRUE " +
            " WHERE c.uuid = :uuid"
    )
    void graded(@Param("uuid") UUID contestUuid);

    @Query("SELECT c " +
            "FROM Contest c " +
            "INNER JOIN Participation p ON c.contestId=p.contest.contestId " +
            "WHERE p.user.userId = :userId")
    Page<Contest> findAllContestsUserParticipatesIn(@Param("userId") int userId, Pageable pageable);

    @Query(value = "SELECT c " +
            "FROM Contest c join Juror j " +
            "WHERE :userId = j.userId and :date  " +
            "BETWEEN c.phaseOne AND c.phaseTwo")
    Page<Contest> findAllContestsInPhaseTwoWhereUserIsJuror(LocalDateTime date, int userId, Pageable pageable);
}
